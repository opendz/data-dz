- la réalisation des villes nouvelles, en intégrantl'ensemble des exigences de développement
durable, notammentle principe d'efficacité énergétique et l'encouragement del'utilisation des
énergies renouvelables et del'investissement, conformémentà leurs plans d'aménagementet

à leurs vocations, ainsi que l'adaptation et la mise à niveau desvilles ;
- la création, à court terme, de nouveaux lotissements sociaux satellitaires et de nouveaux

pôles autour des grandesvilles du Nord et du Sud, pour un meilleur aménagementterritorial et
une meilleure répartition de la population et ce, à travers:

+
e

l'intensification de la création delots sociaux comme moyen de répondre aux demandes en
logements dans les wilayas du Sud et des Hauts Plateaux ;
la création de nouveaux pôles urbains dansle respect de la politique de l'aménagement du
territoire.

Parailleurs, le plan d'action du gouvernement prévoit également :
- l’actualisation du SNATafin de réajuster la stratégie nationale d'aménagement du territoire,
dans le contexte socio-économique et politique actuel ainsi que l'actualisation des Schémas
Directeurs d'Aménagement des Aires Métropolitaines (SDAAM);
- la révision du Plan d'Aménagementdu Territoire de Wilaya (PATW) afin de définir une vision
globale collective et partagée et proposer un cadre de référence pourles futures politiques de
développement et d'aménagement du territoire et de promouvoir une stratégie à moyen et
long termes pourle territoire de la wilaya ;
- l'adaptation du Plan d'Aménagement et de Développement du Territoire de la Commune
(PADTC) afin de renforcerl'efficacité et la cohérence de l'action publique au niveau de cet
espace pour un aménagement de proximité ;
-le rééquilibrage entre les intérêts des populations qui vivent dans les espaces
sensibles (zones frontalières, espaces oasiens, montagnesetlittoral) et la protection de ces
milieux trèsfragiles, à travers des études d'aménagement des espaces ;
- le renforcement du Schéma Directeur d'Aménagement du Littoral (SDAL) qui constitue un outil
technique, institutionnel et juridique, afin de maîtriser la croissance des zoneslittorales et
d'assurer un développementplus qualitatif ;
- la valorisation et l'aménagement des écosystèmes oasiens, à travers des projets intégrantles
secteurs clés pour un développement local adapté aux potentialités des ces territoires
et stabilisation des populations, parl'amélioration de leurs conditions socio-économiques;
- le développement économique durable des massifs montagneux :
- l'identification et le développement des zones à promouvoir et le développement des Schémas
Directeurs des grandesinfrastructures et des servicescollectifs d'intérêt national qui déclinent
la stratégie nationale d'aménagement du territoire aux niveaux sectoriels.
3.2. Respect des règles d'urbanisme et des normes
Dans ce cadre, les actions du gouvemement visent la mise en œuvre d'une véritable politique
d'urbanisme quitient compte des normes architecturales et préservele patrimoine.

Cesactions portent notammentsur :
-la valorisation de la production architecturale, à travers le renforcement du contexte

règlementaire en vue del'émergence d'une architecture et d'une ingénierie de la construction
de qualité, l'élaboration de charte d'identité architecturale régionale et l'encouragement de
l'émergence d'œuvresarchitecturales de qualité ;
57

