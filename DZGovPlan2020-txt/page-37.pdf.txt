Des mesuresincitatives pour les producteurs étrangers seront mises en place afin de faire de
l'Algérie une destination privilégiée pourles tournagesdefilms et de séries.
En outre, des facilités douanières, d'exonérations fiscales et d'avantages bancaires seront

octroyées au profit des producteurs et des investisseurs en matière de production
cinématographique, de distribution de films et d'exploitation des salles de cinéma. Le partenariat
sera encouragé notamment pour la réalisation de studios d'industrie cinématographique, de
studios de tournage et d'enregistrementet de salles de spectacles.
Le gouvernement s’attèlera à accompagner les professionnels du cinéma et à renforcer la
profession par un dispositif réglementant leur vie professionnelle, de même qu'il encouragera
l'émergence de clubs de cinéma et de mouvements associatifs qui seront considérés comme des
partenaires culturels privilégiés.
Concernantlesinfrastructures, le gouvernement favorisera la réalisation de salles de cinéma et de

multiplexes dans les agglomérations et les centres commerciaux, pour promouvoir la
consommation culturelle.
Enfin, le gouvemement appuieral'investissement dans le domaine cinématographique à travers à
la mise en place de mesuresincitatives et d'un dispositif simplifié pourl'accès au financement
bancaire.

2.12 Développementdesinfrastructures d'appui aux technologies de l'information et de la
communication
Eu égard au rôle incontournable que jouent de plus en plus les TIC dans le développement des
activités économiques, le développement des infrastructures d'appui aux Technologies de
l'information constitue un axe important dansla stratégie gouvernementale visant le renouveau de
l'économie nationale.
L'action du gouvernement portera surl'utilisation optimale des infrastructures existantes et la
réalisation de nouvelles capacités répondant aux normes internationales, supports indispensables
à l'émergence et au développement d'uneréelle économie numérique.
Ainsi, le gouvernement œuvrera à :

- améliorer la qualité de service de la connectivité au profit des citoyens et opérateurs
économiques;

- généraliser l'accès à l'Internet très haut débit à travers la modernisation ainsi que la

densification du réseau de télécommunication, la sécurisation des infrastructures des
télécommunications / TIC et la protection desutilisateurs;

-_ ériger l'Algérie en hub régional en matière de télécommunications / TIC par le développement
de Datacenters aux normes internationales et la rentabilisation des capacités nationales des
télécommunications, à savoir la dorsale transsaharienne, les câbles sous-marins et les
capacités satellitaires ;

-_ développer la société algérienne de l'information par la promotion des contenus numériques
locaux et la mise en place des mécanismes de gestion et de gouvernance de l'Internet.

Cesefforts seront accompagnés parl'amélioration de la qualité des services offerts et une plus
grande participation des services postaux à l'inclusion sociale et financière à travers le
développement des services et moyens de paiement électronique et l'encouragement de
l'émergence du commerce électronique.
37

