Préambule
Dans un formidable sursaut, historique, caractérisé par un pacifisme hors du commun, le peuple
algérien a exprimé et affirmé sa forte aspiration pour le changement, la démocratie, la justice
sociale et l’État de droit.
Ces valeurs fondamentales sont à la base des besoins exprimés par les Algériens et les
Algériennes qui sont sortis pacifiquement, le 22 février 2019, pour mettre fin aux errements du
pouvoir d'alors et demander une refonte de la gouvernance de l’État allant dans le sens d'une
réelle démocratie qui permet au peuple d'être la source unique de tous les pouvoirs.
Cette prise de conscience collective est née d'une crise multidimensionnelle qui est elle-même
issue de dérives successives dans la gestion des affaires publiques et d'une mainmise de forces
occultes et extra-constitutionnelles sur les leviers de la décision politique. Cette crise a affecté
lourdement et en profondeur les institutions de L’État par une corruption érigée en système de
gouvernance,fragilisé davantage la cohésion sociale et fait peser des menaces majeures sur notre
souveraineté nationale, tant sur le plan interne que sur le plan externe. L’État national s'est trouvé
en danger par rapport à son intégrité territoriale, sa sécurité nationale et même l'unité de son
peuple.
Faisant sienne cette dynamique historique, le gouvernement veillera à concrétiser, dès
l'aboutissement de la révision constitutionnelle, l'engagement du Président de la République de
revoir, d'une manière profonde,l'ensemble du dispositif d'organisation des élections dans l'objectif
d'en faire un véritable moyen d'expression de la volonté populaire.
Le gouvernement s'engage également à prévoir, dans le projet de révision du dispositif légal
encadrant le régime électoral, un ensemble de mesures dont l'objectif est de favoriser l'émergence
d'une nouvelle génération d'élus, où les jeunes et les femmes occuperont une place de choix, afin
de participer à la gestion des affaires du pays à travers, notamment,les financements sur fonds
publics des campagnes électorales des jeunes.
Les attentes du peuple algérien exprimées à travers ce sursaut pacifique et digne appelant à
l'édification d'une Algérie nouvelle fière de son histoire qui est bâtie sur le sacrifice de millions
d'algériens et d'algériennes, forte par ses institutions refondées selon une vision et un esprit
novateur porteur de modernité, de pragmatisme, de rationalité, de transparence et d'intelligence,
notamment dans le domaine économique et celui de la connaissance.
Le gouvernement entend relever ces défis que traduit parfaitement la vision globale de Monsieur le
Président de la République, vision nourrie des réalités politiques, économiques et sociales et
inspirée des attentes exprimées, de manière claire et déterminée, par les citoyens et citoyennes.
Cette nouvelle République, à laquelle nous aspirons tous, constituera le jalon de l'action du
gouvernement à travers:
1. la consécration d'une démocratie effective et l'instauration d'un nouveau mode de
gouvernance, basé sur la transparence de l'action publique, la moralisation de la vie
politique par une lutte déterminée contre la corruption et les corrupteurs ainsi que la
fondation d'un véritable partenariat avec les acteurs sociaux pour une concertation et un
dialogue permanent, responsable et apaisé ;
2. l'affirmation de l’État de droit, pilier d'une Algérie qui protège les droits et libertés des
citoyens;


