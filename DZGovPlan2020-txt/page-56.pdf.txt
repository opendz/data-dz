- l'achèvementdes travaux de réhabilitation et la remise à niveau technologique de télécabines
et téléphériques;
- l'achèvement des travaux de l'autoroute Est-Ouest, la mise en place des systèmes
d'exploitation et de péage et la transformation progressive, en axe autoroutier, de la Route
Transsaharienne;

- l'achèvement des travaux des liaisons et pénétrantes autoroutières et la sauvegarde du
réseau routier par des projets d'entretien et de réhabilitation ;
- l'introduction de systèmes intelligents de gestion du trafic au niveau des grandesvilles et la
mise en œuvre du programme de modernisation des outils de production des transports des
voyageurs et de marchandises;
- l'amélioration des conditions d'accueil au niveau des gares, le rehaussement du niveau de

qualité des prestations à bord destrains et la mise en œuvre des mesures nécessaires devant
garantir une meilleure ponctualité destrains ;
-le lancement de programmes de réalisation d'infrastructures d'accueil et de traitement de
voyageurs (gares routières, stations urbaines, pôles d'échanges) et l'élargissement du
périmètre géographique de l'offre de transport parle lancement de nouvelles dessertes ;
- l'achèvement du programmed'extension, d'électrification et de modernisation du réseau ferré
en termes de dédoublement de voies, ainsi quela réalisation de nouvelleslignes ;

- l'achèvement des travaux de l'aérogare et de l'aéroport international d'Oran ainsi que les
extensions des infrastructures portuaires;

- la réouverture à l'exploitation de l'ensemble des infrastructures aéroportuaires à la circulation
aérienne avec un renforcementet un renouvellementde la flotte aérienne du pavillon national.
Enfin, le gouvernement œuvrera également à améliorerla qualité de service des transports publics
et l'efficacité des systèmes en place, en termes de fréquence, de durée de déplacements, de
confort et de sécurité à travers, notamment le renforcement des missions des autorités

organisatrices du transport urbain. À cetitre, il s'engage à :

-renforcer le dispositif réglementaire régissant l'activité de transportet moderniser
l'administration par la numérisation des services et la simplification des procédures ;
- réaliser des études de mobilité (plans de transport et de circulation), en vue de répondre
efficacementà la demande de transport d'une manière organiséeet ciblée et pour lutter contre
les phénomènes dela congestion et de la pollution et s'inscrire ainsi dans le développement
durable et l'amélioration de la qualité de vie descitoyens.
3. Pour un cadre de vie de qualité :
3.1. Aménagementduterritoire et projets intégrés
Les objectifs du gouvernementvisent « la mise en œuvre d'unevéritable politique d'aménagement
du territoire » qui assure un environnement urbain et rural préservé, valorisé et propice à
l'épanouissement du citoyen, lui assurant un cadre de vie de qualité, respectueux des exigences
de développement durable.
A cetitre, les actions du plan d'action du gouvernementportent notammentsur:
- le renforcement du dispositif relatif à l'aménagement etl'urbanisme et à la politique dela ville,
pour intégrer toutes les dimensions environnementales, sociologiques et paysagères,
s'appuyant sur les fondements et principes des objectifs de développement durable et du
nouvel agenda urbain mondial;

56

