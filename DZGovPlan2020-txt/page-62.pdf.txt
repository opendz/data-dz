CHAPITRE CINQUIEME
RENFORCEMENT DE LA SECURITE ET DE LA DEFENSE NATIONALE

En matière de défense nationale, l'Armée Nationale Populaire agissant dans le cadre de ses
missions constitutionnelles de défense et de souveraineté nationales ainsi que de préservation de
l'intégrité du territoire national, sous la direction du président de la République, Chef suprême des

Forces Armées, Ministre de la Défense Nationale, poursuivra les efforts entrepris en matière de

modernisation et de professionnalisation de ses composantes.

Les événements qui affectent certains pays frontaliers exigent le renforcementde la sécurisation
desfrontières terrestres du pays, notammentcelles du Sud et du Sud-est et des espaces maritime
et aérien, à travers la mise en œuvre de plans d'action appropriés et une montée en puissance des
moyens humains et matériels, proportionnelle à l'ampleur de la menace, de manière à pouvoir faire
face à toute éventualité.
Le gouvernement soutiendra l'Armée Nationale Populaire, appuyée par les autres services de
sécurité, dans l'intensification, sans relâche, de la lutte contre les résidus du terrorisme jusqu'à

l'éradication définitive de ce phénomèneétranger à nos coutumes et valeurs ancestrales ainsi que
contrele crime transfrontalier,le trafic de drogue,la contrebandeet l'immigration clandestine etce,

dans le strict respect des engagements de notre pays et de son attachement à la promotion de la
paix aux niveaux régionalet international.
Dans ce cadre, la coopération avec les pays voisins en matière de lutte contre le terrorisme et la
subversion, ainsi que la criminalité transfrontalière sous toutes ses formes, aussi bien dans un

cadre bilatéral que multilatéral, sera développée et renforcée, à travers des mécanismes
opérationnels adaptés.
Le gouvernement appuiera le développement des secteurs de l'industrie et de recherchedéveloppement de défense, résolument orientés vers la maîtrise de la technologie et le savoirfaire, qui connaissent une dynamique et une impulsion nouvelle en termesde partenariat diversifié,
de mobilisation de toutes les capacités nationales et une intégration harmonieuse l'outil industriel
national, de nature à contribuer au développement del'économie nationale.
L'Armée Nationale Populaire, guidée par les principes et objectifs de politique étrangère et de
coopération nationale, développera les actions de coopération internationale bilatérale et
multilatérale dans le domaine militaire, dans le but d'accroitre ses moyens en équipements et ses
capacités d'action, pour promouvoir les espaces de sécurité et de stabilité et apporter sa
contribution aux opérations de maintien de la paix, notamment en Afrique, sous l'égide des
organisations internationales.
Le gouvernement renforcera les capacités de cyber défense de l'Armée Nationale Populaire lui
permettant une mise en œuvreefficace de la stratégie nationale de la sécurité des systèmes
d'information pourla protection et la sécurisation de l'ensemble desinstitutions et organismes de
l'Etat contre toute menaceen la matière.

62

