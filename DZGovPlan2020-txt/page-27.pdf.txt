Pour ce faire, le gouvernement veillera à lutter contre la littoralisation des activités économiques en
élaborant une carte nationale des opportunités d'investissements et en ouvrant de nouveaux
espaces de production du foncier économique, notamment dans les Hauts-plateaux et le Sud du pays.

En outre,le gouvernement œuvrera à :
-améliorer la gestion du foncier industriel, en accentuant la lutte contre les lots en déshérence;

- diligenter la mise en service de nouvelles zones industrielles en priorisant la réalisation de celles implantées dans des zones enregistrant un important déficit en matière de foncier;
- aménager de nouvelles zones d'activités économiques et les doter des commodités requises;
- impliquer la réhabilitation des opérateurs économiques des zones industrielles en activité.
En tout état de cause, la réalisation et la gestion des zones industrielles fera l'objet d'une approche nouvelle, à travers la mise en place d'un nouveau cadre institutionnel et juridique redéfinissant les rôles de tous les intervenants. Dans ce cadre, le gouvernement veillera à réprimer sévèrement les tentatives de détournement du patrimoine foncier économique de sa vocation ou d'immobilisation indue sans réaliser les investissements annoncés.

2.4 Développement stratégique des filières industrielles et des mines

Il s'agira de mettre en place un cadre juridique cohérent de promotion de l'investissement

productif, notamment dans:
-les industries agro-alimentaires;

-les industries de matériaux de construction ;
- les industries mécaniques;

- les industries chimiques;

-_ l'électronique et l'électroménager;
-_ l'industrie pharmaceutique ;

- les industries textiles et cuir.

Un effort accru sera plus particulièrement investi pour développer les industries de transformation de matières premières, notamment agroalimentaires, sidérurgiques et en aval des hydrocarbures, avec le concours des investisseurs nationaux et étrangers. Cet effort qui dégagera une plus-value dans la valorisation locale des ressources agricoles, minières et minérales, permettra le développement d'autres branches de l'industrie locale.

Par ailleurs, le gouvernement veillera à réviser les textes réglementaires encadrant le dispositif CKD, dans l'objectif d'augmenter le taux d'intégration nationale dans les activités de montage et d'assemblage, notamment dans les industries mécaniques, électriques, électroniques et de promouvoir les filières de sous-traitance dans ces domaines d'activité.

A cet effet, les investissements dans les processus industriels des activités de sous-traitance bénéficieront d'un cadre incitatif d'accès au foncier et au financement et d'un régime fiscal préférentiel. Les autres produits éligibles à une intégration nationale seront également identifiés et bénéficieront de mécanismes et outils pour leur développement et leur promotion.

27
