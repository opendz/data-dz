# Quand Ammar Belhimer « travaillait » pour la fondation allemande Friedrich-Ebert

*Publié le 16 avril 2020, Par Amélia Guatri*

L’actuel ministre de la Communication Ammar Belhimer a défrayé la chronique cette semaine par son silence sur l’incarcération des journalistes, ses attaques contre les deux sites Maghreb Emergent et Radiom.info d’El Kadi Ihsane et par ses menaces contre la presse en jurant d’ouvrir le dossier de leur « financements extérieurs » !

Issu lui-même du « milieu » de la presse, Amar Belhimer oublie qu’il a « travaillé », au moins entre 2008 et 2010, pour la fameuse Fondation [Friedrich-Ebert-Stiftung (FES)](https://www.fesparis.org/) en réalisant deux études pour lesquelles nous ignorons s’il a été « rémunéré » ou non !

Lire : [Ammar Belhimer accuse Radio M de financements étrangers](https://www.dzvid.com/2020/04/12/ammar-belhimer-accuse-radio-m-de-financements-etrangers/)

En 2008, Ammar Belhimer a produit pour le compte de la Fondation Friedrich-Ebert-Stiftung, une association liée au Parti social-démocrate allemand, une étude intitulée « Le pluralisme politique, syndical et associatif », disponible sur le site de la Fondation.

Cette fameuse incursion dans le monde syndical et associatif algérien avait soulevé à l’époque un tollé et une levée de boucliers contre la Fondation Friedrich-Ebert-Stiftung accusée par Sidi Saïd de « dépasser ses prérogatives et ses missions en Algérie».

Lire : [Radio M condamne « les propos diffamatoires » de Ammar Belhimer](https://www.dzvid.com/2020/04/13/radio-m-condamne-les-propos-diffamatoires-de-ammar-belhimer/)

L’étude « Le pluralisme politique, syndical et associatif » produite par Ammar Belhimer a été une bonne pièce d’artillerie dans cette bataille menée par la Fondation Friedrich-Ebert-Stiftung contre l’[UGTA](https://fr.wikipedia.org/wiki/Union_g%C3%A9n%C3%A9rale_des_travailleurs_alg%C3%A9riens) et contre le gouvernement algérien.

Aussi, la presse de l’époque, des journalistes et des militants politiques avaient pris fait et cause pour la Fondation allemande Friedrich-Ebert-Stiftung contre l’Ugta, Sidi Saïd et le gouvernement.

Lire : [Tous les dossiers de financements extérieurs de la presse nationale seront ouverts](https://www.dzvid.com/2020/04/16/tous-les-dossiers-de-financements-exterieurs-de-la-presse-nationale-seront-ouverts/)

Certaines sources indiquent que ce « travail » de notre actuel ministre de la Communication Ammar Belhimer, qui se plaint des « financements étrangers », a été payé rubis sur l’ongle par la Fondation allemande Friedrich-Ebert-Stiftung liée au Parti social-démocrate !

« **Les politiques de réconciliation nationale** » est le titre de la deuxième étude de Ammar Belhimer financée par cette fondation étrangère et publiée en 2010.

Installé en Algérie depuis 2002, la Fondation Friedrich-Ebert-Stiftung qui dit œuvrer pour « le développement et la promotion de la démocratie » avait réussi à peser lourdement sur les activités de l’opposition algérienne en l’orientant sensiblement.

Amélia Guatri

---

Article paru la première fois [sur DZvid](https://www.dzvid.com/2020/04/16/quand-ammar-belhimer-travaillait-pour-la-fondation-allemande-friedrich-ebert/).