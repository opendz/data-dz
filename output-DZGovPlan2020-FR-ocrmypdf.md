REPUBLIQUE ALGERIENNE DEMOCRATIQUE ET POPULAIRE




       PLAN D'ACTION DU GOUVERNEMENT
    POUR LA MISE EN ŒUVRE DU PROGRAMME
        DU PRESIDENT DE LA REPUBLIQUE




                                        06 Février 2020
                                          SOMMAIRE

PREAMBULE
                             CHAPITRE PREMIER
        POUR UNE NOUVELLE REPUBLIQUE NEE DES ASPIRATIONS POPULAIRES


Un nouveau mode de gouvernance empreint de rigueuret de transparence
     Refonte du dispositif législatif d'organisation des élections
     Moralisation de la vie publique
     Refonte de l’organisation et des modes de gestion de l'Etat et de ses démembrements

Un exerciceplein des droits et libertés
    Liberté de réunion et de manifestation
    Avènementd'une société civile libre et responsable
    Renforcement du dialogue et de la concertation
    Promotion et autonomisation dela femme
    Mise en place d'un plan national de promotion de la jeunesse
    Unejustice indépendante et moderne
    Garantie de la sécurité des personneset des biens
    Uneliberté de la presse et des médias consacrée
    L'affirmation, la promotion et la protection des composantes de l'identité et de la mémoire
    nationales
    Unerelation complémentaire entre les pouvoirs exécutifet législatif


                            CHAPITRE DEUXIEME
          POUR UNE REFORME FINANCIERE ET UN RENOUVEAU ECONOMIQUE


Acterla réforme financière
     Refonte du système fiscal
     Instauration de nouvelles règles de gouvernance budgétaire
     Modernisation du système bancaire etfinancier
     Développement de l'information statistique économiqueet sociale et de la fonction prospective

Impulserle renouveau économique
    Promotion du cadre de développementde l’entreprise
    Amélioration substantielle du climat desaffaires
    Rationalisation du déploiement territorial, du développement industriel et de l'exploitation du
    foncier économique
    Développement stratégique desfilières industrielles et des mines
    Renforcement des capacitésinstitutionnelles en matière de développementindustriel et minier
    Valorisation de la production nationale
    Rationalisation des importations et promotion des exportations
    Assainissement de la sphère commerciale
    Transition énergétique
 <     Agriculture et pêche modernes, pour une meilleure sécurité alimentaire
 +     Pour une véritable industrie du tourismeet de la cinématographie
 +     Développement desinfrastructures d'appui aux TIC
 +     Economie de la connaissance et transition numérique accélérée

Une approche économique pour lutter contre le chômage et promouvoir l'emploi
 *__Adéquation des programmes de formation avec les besoins du marchédu travail
 + Promotion de l'emploi
 + Soutien à la création d'activités

                               CHAPITRE TROISIEME
              POUR UN DEVELOPPEMENT HUMAIN ET UNE POLITIQUE SOCIALE
Développement humain
       Éducation
       Enseignement supérieur
       Formation professionnelle
       Santé et accessibilité aux soins
       Culture
       Promotion des activités physiques et sportives et du sport d'élite
Politique sociale
       Accroissement et consolidation du pouvoir d'achat du citoyen
       Prise en charge des populations vulnérables
       Préservation et consolidation des systèmes de sécurité sociale et deretraite
       Accèsau logement
       Accèsà l'eau potable età l'énergie
       Mobilité et transport

Pourun cadre devie de qualité
 e__   Aménagement du territoire et projets intégrés
 +     Respect desrègles d'urbanisme et des normes
 +     Environnementet développement durable


                                CHAPITRE QUATRIEME
               POUR UNE POLITIQUE ETRANGERE DYNAMIQUE ET PROACTIVE

                               CHAPITRECINQUIEME
              RENFORCEMENT DE LA SECURITE ET DE LA DEFENSE NATIONALE
Préambule


Dans un formidable sursaut, historique, caractérisé par un pacifisme hors du commun, le peuple
algérien a exprimé et affirmé sa forte aspiration pour le changement, la démocratie, la justice
sociale et l'Etat de droit.
Ces valeurs fondamentales sont à la base des besoins exprimés par les Algériens et les
Algériennes qui sont sortis pacifiquement, le 22 février 2019, pour mettre fin aux errements du
pouvoir d'alors et demander une refonte de la gouvernance del'Etat allant dans le sens d'une
réelle démocratie qui permet au peuple d'être la source unique de tous les pouvoirs.
Cette prise de conscience collective est née d'une crise multidimensionnelle qui est elle-même
issue de dérives successives dans la gestion des affaires publiques et d'une mainmise de forces
occultes et extraconstitutionnelles sur les leviers de la décision politique. Cette crise a affecté
lourdement et en profondeurles institutions de l'Etat par une corruption érigée en système de
gouvernance,fragilisé davantage la cohésion sociale etfait peser des menaces majeures sur notre
souveraineté nationale, tant sur le plan inteme que surle plan externe. L'Etat national s'est trouvé
en danger par rapport à son intégrité territoriale, sa sécurité nationale et même l'unité de son
peuple.
Faisant sienne cette dynamique historique, le gouvernement veillera à concrétiser, dès
l'aboutissement de la révision constitutionnelle, l'engagement du Président de la République de
revoir, d'une manière profonde,l'ensemble du dispositif d'organisation des élections dansl'objectif
d'en faire un véritable moyen d'expression de la volonté populaire.

Le gouvernement s'engage également à prévoir, dans le projet de révision du dispositif légal
encadrantle régime électoral, un ensemble de mesures dont l'objectif est de favoriser l'émergence
d'une nouvelle génération d'élus, où les jeunesetles femmes occuperont une place de choix, afin
de participer à la gestion des affaires du pays à travers, notamment,les financements sur fonds
publics des campagnes électorales des jeunes.
Les attentes du peuple algérien exprimées à travers ce sursaut pacifique et digne appelant à
l'édification d'une Algérie nouvelle fière de son histoire qui est bâtie sur le sacrifice de millions
d'algériens et d'algériennes, forte par ses institutions refondées selon une vision et un esprit
novateur porteur de modernité, de pragmatisme, de rationalité, de transparenceet d'intelligence,
notamment dans le domaine économique et celui de la connaissance.
Le gouvemement entend relever ces défis que traduit parfaitementla vision globale de Monsieurle
Président de la République, vision nourrie des réalités politiques, économiques et sociales et
inspirée desattentes exprimées, de manière claire et déterminée, par les citoyens et citoyennes.
Cette nouvelle République, à laquelle nous aspirons tous, constituera le jalon de l'action du
gouvemementà travers:
    1. la consécration d'une démocratie effective et l'instauration d'un nouveau mode de
       gouvemance, basé sur la transparence de l'action publique, la moralisation de la vie
       politique par une lutte déterminée contre la corruption et les corrupteurs ainsi que la
       fondation d'un véritable partenariat avec les acteurs sociaux pour une concertation et un
       dialogue permanent, responsable et apaisé ;
   2. l'affirmation de l'Etat de droit, pilier d'une Algérie qui protège les droits et libertés des
       citoyens;
   3. la séparation des pouvoirs, leur équilibre et la cohérence de leurs actions sont des
      exigences indispensables pour un système stable par la force et la légitimité de ses
       institutions et leur pérennité ;

   4. la consolidation et le développement de la liberté d'une presse plurielle et de médias
       indépendants;

   5. la modernisation de l'Etat et de ses institutions qui s'impose comme préalable au progrès
      voulu pournotre société.

La réorganisation de l'administration centrale et locale ainsi que l'amélioration de la performance
des services publics sont, de ce fait, les actions à entamer en priorité par le gouvernement à
travers la numérisation, l'intervention rationnelle en moyens et, surtout, la formation et la
qualification des ressources humaines, afin d'introduire les standards internationaux dans la
gestion et procédures internes dans tous les secteurs, notamment dans tous les domaines de la
vie économique.

L'accès aux fonctions de responsabilité sera désormais tributaire de la qualification et du mérite et
ce, à travers la mise en place de mécanismes et de procédures d'évaluation et de redevabilité.
Désormais la responsabilité s'octroie par le mérite, la compétenceet l'intégrité.

La nouvelle politique économique et financière constituera le fer de lance de l’action du
gouvemement qui ambitionne, à travers le présent plan d'action, de créer les mécanismes et
l'écosystème adéquats pour la relance du développement et l'émergence d'une économie
diversifiée, pérenne, créatrice d'emplois et de richesses etlibérée du carcan bureaucratique qui
étouffe non seulementles entreprises mais qui bloque, également,la capacité de notre économie
à croitre durablement.

Une politique industrielle rénovée et une agriculture modernisée et une véritable économie dela
connaissance constituent les piliers pour amorcer la croissance, en adéquation avec le grand
potentiel que recèle notre pays.

La création d'emplois et la lutte contre le chômage obéiront, désormais, à une approche
strictement économique, à travers la stimulation des activités rentables et, plus particulièrement,
dans le domaine de la micro entreprise, des incubateurs, des startups et du numérique.

Le gouvernement entend, également, œuvrerà la refonte du système éducatif et de formation qui,
tout en consolidant l'enseignement des valeurs de notre société et de notre patrimoine historique,
s'orientera résolument vers la science et la maitrise des technologies, ce qui permettra une mise à
contribution de nos élites et de notre jeunesse dans le processus d'édification nationale afin de
valoriser toutesles forces vives au service du progrès et du développement de notre pays.

La modernisation du service public de santé ainsi que l'amélioration des soins pour tous,
bénéficieront d'une priorité affirmée dans l’action du gouvernement et cela, à travers le
renforcement des outils de prévention et la garantie d'une pleine couverture et d'une meilleure
prise en charge sanitaires des citoyens à travers tout le pays.

L'éducation et la santé sont les référents permanents pourcette Algérie nouvelle à laquelle nous
aspirons tous.
Le souci d'améliorer les services sociaux se traduit, également, dans les préoccupations du
gouvemement par l'impérative création des conditions d'épanouissement culturel de nos citoyens
et cela, à travers la redynamisation etla généralisation de la vie culturelle chez toutes les franges
de la société et la généralisation de la pratique sportive est également un facteur
d'épanouissement de notre jeunesse à traversl'accompagnement du sport et des sportifs dansles
milieux scolaires et universitaires.
Fortement attaché à la vocation sociale de l'Etat, le gouvemement s'engage à accroître et à
consolider le pouvoir d'achat du citoyen, à veiller à la prise en charge des populations vulnérables
et à la préservation d'un système national de sécurité sociale efficaceet juste.

L'amélioration de la qualité de vie sera une action prioritaire et continue du gouvernement, dont
l’action s'appuiera sur une vision intégrée d'aménagement du territoire, par le respect des règles
d'urbanisme et de l'environnement et l'impérative mise en route des mécanismes de
développement durable.
Surle plan international, le gouvernement adoptera une diplomatie plus active dans les domaines
économiques, culturels et cultuels au service du rayonnement intemational et une diplomatie
basée surles principes de respect du bon voisinage,la non-ingérence dansles affairesintérieures
des autres pays etla priorisation du dialogue dansle règlement desconflits.
Le gouvernement est attaché à une politique étrangère dynamique et proactive qui renforce
l'Algérie dans sa place naturelle et son rôle dynamique surle plan régionalet international.

La communauté nationale à l'étranger est une partie intégrante de la communauté nationale. Elle
est au cœur des préoccupations del'Etat. Le gouvernement sera à son écoute pour garderun lien
affectif avec la mère patrie et la prise en charge des problèmesqu'elle rencontre, à travers la mise
en place de dispositifs permettant à nos sœurs et frères établis à l'étranger de participer
activement au développement de leur pays.
Plus particulièrement, le gouvernement est déterminé à faire appel à toutes les compétences
nationales à l'étranger pour profiter de leur savoir dans le domaine scientifique, technique et
culturel, pour construire cette Algérie nouvelle et s'ouvrir à l'universalité.

Le gouvemement se fonde sur une ligne de conduite et une doctrine qui placent les
préoccupations du citoyen au centre de ses priorités et auquelil exprime son plein engagement à
le servir et à lui assurer une vie décente, dansla paix et la sérénité.

L'Algérie évolue dans un environnement géopolitique régional et international complexe, c'est pour
cette raison qu'en matière de défense nationale et de sécurité nationale, le gouvernement, sous la
direction du président de la République, Chef suprême desforces armées et ministre dela défense
nationale, encouragera et soutiendra les efforts entrepris en matière de modernisation et de
professionnalisation de ses composantes. || appuiera le développement de l'industrie de défense
et son intégration à l'outil industriel national et contribuera ainsi au développement del'économie
nationale.
Enfin, le gouvemement s'engage également à prendre en charge l'ensemble des préoccupations
des citoyens et citoyennes et à leur créer toutes les conditions pour leur participation pleine et
effective au développement du pays.
Ce n'est que par les valeurs d'égalité des chances et de travail entre tous les citoyens algériens
que nous pourrons éliminer un jourla Hogra,l'exclusion et la marginalisation, et rétablir lesliens de
confiance dans notre société,entreles citoyens eux-mêmes et les citoyens et les représentants de
l'Etat.
                                         CHAPITRE PREMIER
         POUR UNE NOUVELLE REPUBLIQUE NEE DES ASPIRATIONS POPULAIRES



Le plan d'action du gouvernement pour la mise en œuvre du programme présidentiel s'inscrit dans
une conjoncture particulière, qui nécessite la mobilisation de tous pourfaire face aux grands défis
que notre pays doit relever, à leur tête l'instauration d'une nouvelle République répondant aux
aspirations légitimes du peuple et dont les fondements seront basés sur l'enracinement des
principes démocratiques et la protection des droits et deslibertés, dans le respect de la loi et où
chacun jouira, sur un pied d'égalité et sans aucune discrimination, de la sécurité, dela liberté, de la
dignité et de l'égalité des chances ; droits qui devront aller de pair avec les obligations que
commandela citoyenneté.

1. Un nouveau mode de gouvernance empreint de rigueur et de transparence

Le mode de gouvernance desinstitutions publiques adopté jusque-là a montré ses limites pour
assurer aux citoyens les progrès sociaux, économiques et politiques, à la hauteur des besoins et
des aspirations de la société.

Partant, le gouvemement engagera une vaste entreprise de réformes radicales dontla finalité est
l'instauration d'un mode de gouvernance rénové et moderne, qui constituera le support solide à
l'émergencede la nouvelle République.

Cette démarche, qui marquera unerupture radicale avec les anciens modes de gouvernance ayant
conduit à de graves déviations et dérives, s'articulera autour des axes ci-après.

1.1.   Refonte du dispositif législatif d'organisation des élections

Le gouvernement veillera à concrétiser, dès l'aboutissement de la révision constitutionnelle,
l'engagement du Président de la République de revoir d'une manière profonde, l'ensemble du
dispositif d'organisation des élections dansl'objectif d'en faire un véritable moyen d'expression de
la volonté populaire.

Le gouvernement compte ainsi renforcerle caractère inclusif de l'autorité nationale indépendante
des élections dans le souci de consolider le processus démocratique du pays, à travers
l'enracinement des principes de neutralité, de régularité, de transparence et de crédibilité des
opérations électorales.

La refonte du dispositif législatif encadrant les élections permettra de consacrer définitivement le
retrait de l'administration du processus d'organisation des élections, à travers la mise en place des
mécanismes nécessaires à même de garantir l'indépendance totale de cette autorité vis-à-vis du
pouvoir exécutif et judiciaire et dont les prérogatives en la matière, seront davantage renforcées.

Le gouvernement veillera, en outre, à ce que le nouveau dispositif législatif consacrela rigueur et
l'harmonie des critères et des conditions exigées de l'ensemble des candidats aux élections,
qu'elles soient locales ou nationales.
Il s'engage également à prévoir, dans le projet de révision du dispositif légal encadrantle régime
électoral, un ensemble de mesures dont l'objectif est de favoriser l'émergence d'une nouvelle
génération d'élus compétents et intègres, où les jeunes et les femmes occuperont une place de
choix, afin de participer à la gestion des affaires du pays à travers, notamment,les financements
surfonds publics des campagnes électorales des jeunes.

1.2.   Moralisation de la vie publique

Le gouvernement compte inscrire la lutte contre la corruption dans toutes ses formes, la
moralisation et la transparence de l'action publique, au cœur du processus de réforme des
services del'Etat qu'il devra concrétiser.

L'enjeu d'une telle démarche visant à garantir l'impartialité, l'objectivité et la probité des personnes
exerçant les plus hautes responsabilités publiques, est déterminant pour garantir, non seulement
l'Etat de droit, mais plus généralement le renforcement de la confiance des citoyens dans leurs
institutions.

La préventionetla lutte sans merci contre la corruption

Au-delà d'être une pratique juridiquement illégale et éthiquement malhonnête, la corruption a
causé des dommages considérables à l’économie, entaché l'acte politique et altéré la relation
entre le citoyen et l'Etat.

Le gouvernement mènera une lutte résolue contre la corruption, le népotismeetle clientélisme. Il
fera de cette lutte Un engagementferme et un axe d'action prioritaire, qui portera sur:

- la révision des dispositions de la loi relative à la lutte contre la corruption, dans l'objectif
   notamment d'une meilleure fonctionnalité des procédures de saisie et de gestion de biens
   placés sous-main dejustice à traversla création d'une agence nationale dédiéeà ceteffet ;
-_ la requalification de la notion de transaction dans certainesaffaires de corruption impliquantles
   personnes morales, dansl'objectif d'un recouvrement rapide des biens détournés;
- la mise en place de dispositions législatives concernant la protection des lanceurs d'alerte et
   d'un dispositif complémentaire concemantles conflits d'intérêt dans les secteurs public et privé
   et la limitation du nombre de mandats dans les organisations professionnelles et les
   associations à caractère non politique, financées parle Trésor public ;
- le durcissement des peines pécuniaires prononcées contre les personnes morales dans les
   infractions de corruption ;
- la reconsidération du privilège de juridiction, pour consacrer le principe de l'égalité devant la
   justice ;
- la définition et la consécration de la notion d'activité de gestion génératrice de responsabilité
  pénale dansles dispositions pénales en tenant compte du principe de légalité et protégeantles
  biens publics et privés;
- le durcissement du dispositif de lutte contre le blanchiment d'argent;
- la mise en place de nouvelles règles garantissant la transparence des financements des
  campagnes électorales et des partis politiques dont les comptesferontl'objet d'une publication
  officielle.
L'enracinement des valeurs d'éthique et la rénovation du cadre juridique applicable aux
agents publics

De nouveaux mécanismes de prévention et de contrôle, pourgarantir l'intégrité des responsables
publics et une gestion saine des deniers de l'Etat, seront mis en place par le gouvernement, qui
œuvrera à instaurer, dans les faits, une relation rénovée entre le citoyen et l'Etat, bâtie sur la
notion de redevabilité et de reddition des comptes et ce, par des mécanismes juridiques et
opérationnels susceptibles d'assurer la moralisation et la transparence del'action publique et la
prémunir de toute dérive.

Le gouvernement prévoit, dans ce cadre, la mise en place d’un dispositif juridique qui déterminera,
pour ce qui concerne les membres du gouvemement et les hauts responsables publics, les règles
defond, de procédure et de comportement, à même de prévenir les conflits d'intérêts dans la vie
publique.

Le gouvernement entend également rénoverle cadre déontologique applicable à l'ensemble des
agents publics, encadrant le cumul d'activités des fonctionnaires, la prévention des conflits
d'intérêts ainsi quel'élargissement des obligations déclaratives à de nouvelles catégories d'élus et
d'agents publics. Il prévoit aussi l'instauration de nouvelles règles afin de bannir l'inamovibilité des
responsables, source de dérives et d'abus.

La compétence comme critère fondamental d'accès aux fonctions del'Etat

Le gouvernement compte ériger la compétence et l'intégrité comme critères fondamentaux dans
l'accès aux différentes fonctions de l'Etat. Il prévoit à ce titre l'encadrement rigoureux des
recrutements dans l'administration, qui s'effectueront désormais sur la base de la méritocratie, à
travers notammentla généralisation (i) des concours comme moyen d'accèsà la fonction publique
et (ii) de l'appel à candidature pourles nominations aux postes de responsabilité.

Le gouvernement œuvrera, à travers la mise en place des mécanismesjuridiques et statutaires, à
garantir l'indépendance des fonctionnaires vis-à-vis du pouvoir politique.

Il veillera à la mise en adéquation de la rémunération avecla qualité du travail attendu et favorisera
une gestion des ressources humaines sur des bases davantage professionnelles
qu'administratives. Il s'agit notamment d'adapter le cadre juridique afin d'élargir les possibilités
pour l'État de recruter, à différents niveaux, les compétences dont il a besoin, issues d’autres
secteurs quela fonction publique.

Le renforcement de la transparence dans la gestion des finances publiques

Le gouvernements’attèlera à garantir davantage de transparence dans la gestion des finances
publiques et des marchés publics, notammentà travers :

- la garantie de la transparencedansl'accès à la commande publique ;
- une plus grande traçabilité des actes de gestion des deniers publics pour garantir, outre la
   transparence,l'efficacité de l'action publique ;
-_ l'élargissement de la règle de transparence et traçabilité des actes de gestion aux entreprises
   afin delutter contrela fraude,l'évasion fiscale et la corruption ;
- la promotion de la transparence budgétaire par la publication périodique des rapports
  budgétaireset financiers selon les normes internationales.
                                                   9
L'amélioration du système de suivi, de contrôle et d’audit

Le gouvernements'engage dans ce domaineà :

-_ renforcer les missions des organes de contrôle de l'Etat, notamment la Cour des Compteset
   l'inspection générale desfinances et les doter en moyens suffisants:
-_ réorganiser les organes de contrôle, en orientant leurs missions vers la prévention contre les
   situations d'incompatibilité, de conflits d'intérêts et toute autre situation ayant trait à la
   moralisation del’action publique et à la protection des deniers publics;
-_ renforcerles dispositifs d'audit et d'évaluation et les élargir à l'ensemble des administrations et
   établissements publics.

L'ouverture des données publiques

Le gouvernement envisage de rendre accessibles les données publiques, au service d'une plus
grande transparence, responsabilité et efficacité de l’action publique.

La politique du gouvernement pour l'ouverture des données publiques « Open data » témoigne de
sa volonté à mettre en place un système performant de production de données de statistiques
publiques.

Cette politique sera encouragée en renforçant lesincitations à la mise à disposition de données
parles administrations.

Outre les objectifs de transparence et d'efficacité recherchés, cette démarche vise également à
réunir les conditions nécessaires au développement d'une économie numérique qui repose, entre
autre,surl'exploitation des données.

La lutte contre les procédures bureaucratiques et promotion de la démocratie participative

Pour lutter contre la bureaucratie, le gouvernement compte promouvoir et développer
l'administration numérique, tout en accélérant la dématérialisation des services publics qui
demeure un remarquable moyen de transparenceet d'amélioration de l'efficacité et de la proximité
del'action publique.

Le gouvemementprévoit de mettre en place de nouveaux mécanismes permettant à la société
civile de contribuer pleinement à la construction du bien commun et au citoyen d'être associé à
l’action publique. Il œuvrera à créer les conditions idoines pour un échange constant avec les
citoyens, les associations, les entreprises et les syndicats, permettant ainsi l'identification des
problèmeset l'évaluation des politiques publiques.

Cette démarche vise à promouvoir la démocratie participative par l'ouverture de la décision
publique, afin qu'elle soit mieux concertée, mieux débattue, plus collective et plus juste, plus
efficace et mieux comprise.

1.3.   Refonte del’organisation et des modes de gestion del'Etat et de ses démembrements

Le gouvernementinscrit la refonte del'organisation et des modes de gestion del'Etat, comme axe
prioritaire de son action. Cette refonte se traduira par des actions qui toucheront, d'une manière
progressive et graduelle, l'ensemble des entités publiques centrales et territoriales.


                                                  10
Cette réforme concourra à assurer l'avènement d'un Etat de droit au service du peuple, un Etat
moderne,efficace et légitime par sa performance et un Etat stratège promoteur de développement
et garantdel'intérêt général.

Refontede l’organisation de l’administration publique

La refonte de l'organisation de l'administration qui vise la pertinence et l'efficacité de l’action
publique sera basée sur les axes suivants:

-   la réorganisation, la réhabilitation et la modernisation des grands services de l'Etat, des
    administrations centrales et des établissements publics, afin de les adapter aux exigences de
    l'efficacité et de l'efficience. Cet objectif sera atteint à travers la normalisation de la
    structuration, la hiérarchisation des structures et l'harmonisation des missions de chaque entité
   selon un schéma directeur d'organisation et de modernisation adapté ;
-_ la réhabilitation des organismesd'aide à la gouvernance, notammentles autorités de régulation,
   en consolidant leur indépendance et en les dotant des moyens nécessaires à
    l'accomplissement de leurs missions ;
- une meilleure définition des prérogatives et des compétences respectives de l'Etat et des
  collectivitésterritoriales qui s'opérera dans une approche de complémentarité ;
- une redéfinition du rôle de chaque échelon territorial (commune, daïra et circonscription
  administrative) ;
- la réorganisation du réseau déconcentré de l'Etat pour mieux répondre aux priorités du
    gouvernement;
- la révision profonde du code de la commune etdela wilaya, basée sur l'approfondissement de
  la décentralisation et le renforcement des prérogatives des collectivités territoriales et de leurs
    ressources.
-_ la mise en place effective des wilayas nouvellement créées;
-_ l'élaboration d'un nouveau découpage administratif visant à transformerl'architecture territoriale
   du pays, à travers la création de nouvelles communes, afin d'assurer une meilleure prise en
   charge des besoins des citoyens et répondre aux mutations socio-économiques du pays.
- la mise place d'une organisation administrative appropriée de la Capitale et des grandes
   métropoles du pays, à même à assurer une gestion efficace et moderne de nos grandesvilles.

Refonte des modes de gestion de l'administration publique

S'agissant de la refonte des méthodes de gouvernance des administrations publiques, elle sera
conduite selon la démarche et les principesci-après:

- la lutte contre le gaspillage induit par l'adoption d'une approche rentière du développement;
- la réorientation des ressources publiques vers la satisfaction des besoins prioritaires des
    citoyens ;
- la diminution du train de vie de l'Etat, afin de reconquérir la confianceaffectée du citoyen dans
    sesinstitutions ;
- la rationalisation des dépenses, en favorisant la mutualisation des moyens pour gagner en
    efficience;
-_ la promotion et le développement d'un management public capable de s'adapter aux évolutions
    sociales et économiques;
-   la valorisation de l'autonomie des managers et des administrations ;




                                                  11
- la qualification de la ressource humaine à travers l'adaptation de l'appareil de formation,
  l'allocation des ressources financières nécessaires, la refonte des programmes et des
  méthodes pédagogiques et leur adaptation aux exigences de la modernisation ;
- la numérisation et la dématérialisation des différentes prestations publiques .

Dans un contexte de contraintes budgétaires, il est important d'évaluerl’utilisation de chaque dinar
dépensé par le budget de l'Etat. C'est pourquoi le gouvernement s'emploiera à la mise en place
d'une gouvernance appropriée et rénovéede l'évaluation des projets et des politiques publiques,
qui sera externe à l'administration et qui impliquera la consultation des bénéficiaires des projets et
des politiques, des autres institutions publiques, des élus et des partenaires sociaux, et dont les
résultats seront pris en compte pourcorrigeret faire évoluerces politiques.

2. Un exercice plein des droits et libertés

Dansle cadre de la consolidation del'Etat de droit et del'instauration de la nouvelle République,le
gouvernement procédera à la révision d'un ensemble de textes législatifs afin de consacrer les
droits et libertés et ce, à la lumière des nouvelles dispositions qui seront consacrées dansla future
Constitution du pays.

2.1. Liberté de réunion et de manifestation pacifique

Leslibertés de réunion et de manifestation pacifique constituent les principaux socles pourle
renforcement et la consolidation des libertés démocratiques, qui couronneront le processus de
réformespolitiques profondes lancées depuisl'élection du président de la République.

Dans ce cadre, le gouvernement révisera en profondeurle dispositif légal encadrant ce droit, en
renforçant les garanties de l'exercice de la liberté de manifestation pacifique en tant qu'acte de
civisme exercéloin de toutes formes de violence et dansle respect deslois de la République.

En tenant compte des évolutions politiques et sociales qu'a connues notre pays, il sera procédé à
l'adaptation du cadre juridique régissant les réunions publiques, qui seront désormais soumises au
régime dela déclaration.

2.2. Avènement d’une société civile libre et responsable

Le gouvernement concourra avec détermination à l'avènement d'une société civile libre et
dynamique, capable d'assumer pleinement son rôle d'accompagnateur et d'évaluateur del'action
publique au service exclusif du citoyen.

Pour cefaire, le gouvernement compte, à la lumière de la révision de la Constitution, procéder à
l'amendement de la loi relative aux associations et favoriser l'émergence d'une société civile libre
et responsable.

Cette refonte, qui vise à renforcerle rôle des associations et leur participation dans le processus
de développement du pays et la gestion desaffaires publiques, s’appuiera principalementsur:
-_ la consécration du principe de la déclaration dans la constitution des associations ;
-_ l'adoption de nouveaux mécanismes definancement transparents et efficaces;
- le renforcement du partenariat entre les instances publiques et le mouvement associatif,
   notamment dans les domaines d'intérêt général;
- la promotion dela place dela jeunesse et de la femme dansla vie associative.
                                                  12
Parcette nouvelle approche, le gouvernement compte instaurer une réelle et efficace démocratie
participative où le citoyen, parle biais du mouvement associatif, sera acteuret finalité de l'action
publique.

2.3. Renforcement du dialogue et de la concertation

Partant de son fort attachement aux valeurs de dialogue et au respect des engagements pris
devantle peuple, le gouvemement s'engage à érigerla concertation aveclesdifférents partenaires
sociaux commeoutil principal de gouvernance.

Le dialogue et la concertation se feront avec l'ensemble des acteurs politiques, sociaux et
économiques, dans un esprit de participation et de partenariat. C'est dans ce cadre que seront
exposées et débattues en toute franchise et transparence les principales questions économiques
et sociales du pays dans un esprit de partenariat, de coopération et de compréhension.

Conscient que l'apaisement et la stabilité constituent les préalables de la réalisation du projet
ambitieux de renouveau engagé par le président de la République, le gouvernement, mobilisera
toutes ses forces afin de lui assurer les conditions favorables d'une rapide mise en œuvre et de
réussite,

Le gouvemement, dans sa ferme volonté de mettre notre pays sur une trajectoire de
développement durable, aura besoin du soutien de tous les algériens acteurs pour bâtir l'Algérie
nouvelle etla participation de tous ceux quifontla richesseetla diversité de notre société.

Le gouvernements'engage, dans le cadre du dialogue et dela concertation, à réhabiliter le Conseil
National Economique et Social dans son rôle de conseiller du gouvernement,offrant ainsi un cadre
approprié de participation de la société civile à la concertation nationale sur les politiques de
développement économiqueet social et leur évaluation.

2.4. Promotion et autonomisation de la femme

Le gouvemementveillera à réunir toutes les conditions concourant à donnerà la femme algérienne
toute sa place dans l'espace politique, au-delà du système de quota. Dans ce cadre, il œuvrera à
encourager la promotion de la femme aux responsabilités dans les institutions de l'Etat et
administrations publiques.

Devant la dépendance, la précarité, les violences, le harcèlement et l'exploitation dont sont
victimes une certaine catégorie de femmes, le gouvernement est déterminé à mettre en place les
mécanismes qui permettront d'assurerleur protection et autonomisation et en particulier :
- prévenir et combattre toutesles formes de violenceà l'égard des femmeset mettre un terme à
  l'impunité pour les coupables de ces actes;
- mettre en place des services psycho-sociaux dédiés aux femmes vulnérables et assurer un
  accompagnementfinancierdestiné à leur réinsertion sociale ;
- assurerl'autonomisation financière en garantissant un accès facile et simplifié aux incitations
  publiques de création d'emplois et/ou d'activités économiques ;
- faciliter et garantir l'accès au système d'assurance et de sécurité sociale.

Dans le même sillage, le gouvernement œuvrera à l'atteinte de l'objectif de l'égalité hommes-
femmes dans tous les domaines, en conformité avec les constantes nationales.

                                                 13
2.5. Mise en place d’un plan national de promotion dela jeunesse

La promotion de la jeunesse constitue une des grandes priorités du mandat présidentiel et de
l'agenda du gouvernement. Elle fera l’objet d'un Plan national quinquennal 2020-2024, à la faveur
d'une approche transversale, partenariale et multisectorielle.

L'implication de la Jeunesse dans le processus de développement du pays, en sa qualité de force
sociale potentielle, sera concrétisée par la mise en place d'une politique nationale novatrice,
inclusive et intersectorielle, prenant en charge les préoccupations et attentes des jeunes. L'objectif
est d'en faire désormais un acteur et un partenaire à part entière et récipiendaire dans le
processus d'édification de la nouvelle République.

A ce titre, le gouvernement œuvrera à réduire les écarts induits par les différents dispositifs
institutionnels et à unifier et optimiser les efforts en faveur de la jeunesse pour une plus grande
efficience des politiques publiquesliées à la promotion du jeune.

Il veillera, à traversla refonte de ses dispositifs pédagogiques, à promouvoir l'éducation citoyenne,
l'autonomisation etl'émancipation scientifique, technologique, culturelle et artistique des jeunes.

La conception d'un plan national de promotion de la jeunesse, qui ne peut se faire sans la
participation effective du principal acteur à savoir le jeune, prendra comme appui un diagnostic
objectif sur la jeunesse, à travers l'organisation d'espaces consultatifs au niveau local, à l'effet
d'assurer l'appropriation par les protagonistes de la nouvelle politique de la Jeunesse.

Un cadre de concertation approprié, qui réunira tous les secteurs et acteurs concernés par la
jeunesse, sera mis sur pied, pour proposer des mesures concrètesvisant à réformer en profondeur
l’action publique destinée aux jeunes en s'appuyant notamment sur les axes stratégiques
suivants : citoyenneté et engagement dans la vie publique, formation, emploi et entreprenariat,
accès au logement, au sport, à la culture, aux loisirs, à la technologie et au numérique.

L'installation du Conseil Supérieur de la Jeunesse qui s'érigera en porte-voix de la jeunesse
algérienne, en partenaire et interlocuteur privilégié des pouvoirs publics et en instrument efficace
de son implication effective dans la vie politique et socioéconomique du pays, confortera de
manière déterminante la nouvelle stratégie nationale conçue pourl'émancipation et la promotion
dela Jeunesse, en mettantl'accent surle principe del'égalité des chances.

S'agissant des différents leviers pédagogiques, notamment les établissements de jeunesse et de
sport et de proximité, des mesures urgentes seront prises en leur faveur en termes derévision et
de modernisation du mode de gestion, des contenus de programmes, ainsi quele renforcement de
leur encadrement pédagogique et sa mise à niveau. Cette redynamisation s'appuiera sur un
partenariat avec le mouvementassociatif de jeunesse.

Les programmes pédagogiques doivent viser le renforcement de la citoyenneté et la cohésion
nationale, notamment les colonies de vacances en faveur des jeunes du sud et des zones
défavorisées, la mobilité et le brassage des jeunes des différentes wilayas du pays, la promotion
du volontariat et le tourisme des jeunes etl'ouverture de la jeunesse sur le monde et son contact
avec les évolutions universelles. Ils prennent en charge également les préoccupations liées à la
prévention etla lutte contreles fléaux sociaux.

Toutes ces actions convergeront à préparerla jeunesse à reprendrele flambeau et à assumer ses
responsabilités aux plans politique et socioéconomique.

                                                  14
3. Une justice indépendante et moderne

Le plan d'action du gouvernements'inscrit en droite ligne du programme présidentiel qui place la
consécration de l'indépendancede la justice au cœur de sespriorités.

A l'appui de ce fort engagement, et en conformité avec la prochaine révision constitutionnelle, le
gouvernement procédera aux réaménagements appropriés de l'arsenal législatif visant notamment
à renforcerle droit de la défense, à assurerle respect du principe de proportionnalité des peines, à
limiterle recours injustifié à la détention provisoire et à prévenirles erreursjudiciaires.

Il s’agit principalement du :

3.1. Renforcement del'indépendance de la Justice

Une révision sera initiée du statut de la magistrature et dela loi régissant le Conseil Supérieur de
la Magistrature ainsi que de la Charte de déontologie de la profession de magistrat et de la
fonction d'inspection; l'objectif visé est la consécration de l'indépendance de la magistrature et de
la moralisation du travailjudiciaire.

Aussi, des mécanismes seront mis en place afin de protégerl'indépendance du magistrat et de
son intégrité et de valoriser son statut. La révision du système de recrutementet de formation des
magistrats sera également entreprise, en vue de garantir une meilleure protection de la société et
la sauvegarde desdroits et libertés.

3.2. Amélioration dela qualité de la décision dejustice

Outre l'affirmation de la présomption d'innocence et du caractère exceptionnel de la détention
provisoire à travers la mise en place de nouvelles règles et des mesures de contrôle,il sera
procédé à l'introduction de dispositions permettant l'adaptation de notre législation au
développement du contentieux et aux nouveaux types de criminalité et ce, par:

- la révision de la procédure de comparution immédiate ayant généré l'augmentation du taux de
  la détention provisoire, de sorte à la faire coexister avec la procédure de flagrant délit qui sera
    réintroduite ;
-_ l'introduction de nouveaux modes de règlement du contentieux en matière pénale tels que le
   plaider coupable, l'extension de l'ordonnance pénale à toutes les contraventions, la
   généralisation de la transaction dans la loi relative aux infractions de change et aux
   mouvements de capitaux, ainsi que la médiation extrajudiciaire en matière civile :
- la révision de certaines dispositions régissant les tribunaux à compétenceterritoriale étendue,
    ciblant ainsil'efficacité en matière de direction des investigations ;
-_ l'extension des mesures de correctionnalisation législative pour certains crimes afin d'apporter
   une réponse pénale plus rapide ;
- la révision des dispositions du recours en cassation contre les arrêts de la chambre
   d'accusation ordonnant le renvoi au tribunal criminel, pour un meilleur respect de la
   présomption d'innocenceetla diminution de la détention provisoire;
-   la révision des dispositions relatives au tribunal criminel, concemant sa composition, la nature
    de ses décisions et ses modes de saisine et ce, en vue de gagnerenefficacité ;
- la révision des procédures de notification en matière pénale ;
-_ la généralisation dela visioconférence dansles procéduresjudiciaires ;

                                                    15
- l'adaptation du découpage des juridictions administratives et la décentralisation de la gestion
  administrative et financière des juridictions ainsi que l'introduction de la fonction d'audit
     financier;
- le renforcement du rôle de la Cour Suprême et du Conseil d'Etat en matière d'unification et de
  diffusion dela jurisprudence;
- la généralisation du recours aux peines altematives à la prison, notamment le travail d'intérêt
  général.

3.3. Facilitation del'accès à la justice

Outre la généralisation de la numérisation aux différentes phases du dossier judiciaire, le
Gouvernement prévoit l'extension, au profit des usagers dela justice, y compris notre communauté
à l'étranger, des prestations électroniques et de l'information à distance, la suppression des
procédures de dépôt des différents dossiers, dans le cadre d'une démarche visant la numérisation
globale et la suppression progressive du support papier.

Aussi, et dans l'objectif d'affermir l'égalité devant la justice, il sera procédé à la suppression de
l'obligation de la constitution d'avocat dans certains contentieux, notamment en faveur des non-
appelants.

3.4. Amélioration de la performance du système pénitentiaire :

Dans ce domaine,le gouvernement procédera à :

- la révision du code de l'organisation pénitentiaire et ce, en individualisant davantage la peine et
  en aménageant le mode de réduction de la peine afin d'inciter à la discipline et d'encourager
     l'insertion sociale ;
- l'introduction du système de surveillance électronique (bracelet électronique) comme peine
     alternative à l'emprisonnement;
-_ le renforcement en personnel pénitentiaire et l'humanisation des conditions de détention.

 4. Garantie de la sécurité des personnes et des biens

Pierre angulaire del'État de Droit, le renforcementde la sécurité des personnes et des biens est
placé au centre de l'action du Gouvernement, qui entend renforcer les moyens humains et
matériels des services de sécurité pour contrecarrer toute velléité de porter atteinte aux droits des
personneset de la société.

Il s'attachera à renforcerla lutte contre la criminalité sous toutes ses formes (blanchiment d'argent,
crime organisé, cybercriminalité,trafic de stupéfiants. etc.) à travers :
-_   la modernisation des moyens et méthodes de lutte contre la criminalité ;
- une meilleure couverture sécuritaire à travers le territoire national par un maillage adapté ;
- la consolidation de la formation des ressources humaines pour répondre aux objectifs de
     professionnalisme ;
-_ l'anticipation et la prévention des risques et menaces ;
- l'adaptation des méthodes aux nouvelles formes de criminalités émergentes, notamment la
     cybercriminalité ;
-_ le renforcement de la coordination entrelesinstitutions et les services chargés dela protection
   des personnes et des biens.

                                                  16
Par ailleurs, le gouvernement mettra en place une nouvelle politique de prévention et de lutte
contrel'insécurité routière qui s'appuiera surl'introduction effective du système de permis à points,
l'utilisation généralisée du chronotachygraphe, le durcissement des conditions d'obtention des
permis de conduire pourles véhicules de transport public de marchandises et de voyageurs ainsi
quel'intensification des actions de formation et de sensibilisation.

Enfin, et outre le renforcement des effectifs et moyens de la protection civile pour une meilleure
couverture opérationnelle du territoire national face aux risques et catastrophes, il sera procédé à
la promotion et à la vulgarisation de la culture de la prévention desrisques chezle citoyen.

5. Une liberté de la presse et des médias consacrée

Dans cette nouvelle séquence historique exceptionnelle que vit notre pays, étape d'évolution
cruciale vers la construction d'une nouvelle République, la communication est de toute évidence
un immense défi à relever, d'autant plus grand qu'il s'agit d'assurer désormais la promotion des
réformes constitutionnelles, institutionnelles et des politiques publiques, chantiers immensesde la
nouvelle République à laquelle œuvre le président de la République depuis son élection le 12
décembre 2019.

La numérisation accrue dela production et de la transmission des informations, notammentvia les
réseaux sociaux, a rendu obsolètes les méthodes traditionnelles d'information et de
communication au sein des institutions, des entreprises et dans la société, d'où l'impérieuse
obligation de répondre à l'urgence numérique en révolutionnant les modes de pensée et les
méthodes d'informer et de communiquer.

Pour répondre avecintelligence et méthode à l'urgence numérique, le gouvernement ouvrira un
vaste chantier pour mieux élargir le champ dela liberté d'expression, dela liberté de la presse et
dela liberté de communiquer. L'espace deslibertés, c'est le champ du possible, c'est-à-dire un
exercicelibre et apaisé de ceslibertés, loin des multiples atteintes à ceslibertés qui sont générées
parl'évolution exponentielle del'Internetetl'expansion des réseaux sociaux.

Les dépassements et autres abus et atteintes aux personnes physiques et morales, notamment la
facilité à diffamer et à produire de fausses nouvelles à grande échelle, imposent de définir des
règles claires et efficaces encadrant par la force du droit l'obligation éthique de fournir une
information vérifiable, sourçée,fiable et crédible.

La transformation profonde du secteur de l'information et de la communication exige du
gouvemementle lancement de réformes majeures, inclusives et concertées. Il s'agit de procéder à
unerévision substantielle des lois relatives à l'Information et à l'audiovisuel, avec notamment une
mise en conformité avec le droit algérien des chaines de télévision offshore et l'encouragement de
la création de radios dans l'espace hertzien et sur le Web.

Comme il sera question de fixer des cadres légaux pourla publicité, les sondages et la presse
numérique. Le développement d'une presse de proximité, proche desréalités desterritoires et de
leurs populations, sera fortement encouragé.

De même,il sera question d'aider les acteurs de l'information et de la communication à s'organiser
dans le cadre d'associations et de syndicats professionnels et à soumettre le libre exercice
d'informerà l'exigence éthique qui sera défendue dans des cadresorganisés.

                                                      17
6. L’affirmation, la promotion et la protection des composantes de l'identité et de la
   mémoire nationales

Convaincu que la reconnaissance de l'ensemble de l'héritage culturel et linguistique de notre
peuple constitue le ciment del'unité nationale, le gouvernement inscrit comme axe prioritaire dans
son plan d'actionl'affirmation, la promotion et la protection des composantes de l'identité nationale
que sontl'islam, l’arabité et l'amazighité ainsi que le renforcement desliens de notre peuple avec
son histoire et sa culture ancestrale.

La prééminence accordée par le gouvernement à l'affirmation des composantes de l'identité
nationale ira de pair avec la promotion des valeurs de modération, de tolérance et de dialogue et
l'ouverture surles cultures, les civilisations humaines et surles langues vivantes.

Par ailleurs, et face à la recrudescence du discours de la haine et de l'incitation à la fitna, qui
constituent une menace réelle sur l'unité nationale, le gouvernement inscrit comme priorité
l'initiation d'un projet deloi criminalisant toutes formes de racisme, de régionalisme et de discours
dela haine.

6.1. Consolidation et promotion du réfèrentreligieux national
Le gouvernement entend consolider les assises du référentreligieux national et à en renforcerles
fondements par le biais de la promotion d'un discours religieux empreint de modération,
d'humanisme, d'harmonie sociale et de juste milieu. 1! entend tout autant œuvrer à diffuser la
culture islamique authentique et à répandrel'esprit de tolérance, de dialogue et de participation
sociale et de parerà toutesles formes d'extrémisme, de discrimination et de haine.

Le gouvernementveillera également à préserverl'action cultuelle de toute dévianceet s'attèlera à
renforcerle réseau desinfrastructures cultuelles, afin de consoliderla position de la mosquée, de
l'école coranique et des zaouia, les espacescultuels, spirituels et culturels en vue de soutenir les
saines assises de la société algérienne et de conforterle rôle du réseau national desinstitutions
dévolutaires de la gestion de l'action cultuelle. Dans ce cadre, la mosquée Djamaa El Djazair
constituera un pôle de rayonnementspirituel et scientifique.

Il s'agira aussi de renforcerla place des pratiquesreligieuses ancestrales de la société algérienne
afin de favoriserl'influence religieuse modérée et tolérante et de propager la paix en Afrique et
dans d'autres pays du monde.

Parailleurs, le gouvernement œuvrera à promouvoir les biens Wakfs et à renforcerle dispositif de
Zakat en vue de leur permettre de contribuer davantage dans la consolidation de la cohésion
sociale.

Enfin, le gouvernementfourniral'effort nécessaire pourla consolidation dela politique de formation
des imams, afin d'assurer un encadrement optimal du réseau national des mosquées et des écoles
coraniques. Le processus d'encadrement religieux destiné aux membres de la communauté
nationale l'étranger sera davantage soutenu afin de consolider leursliens avecla patrie.

6.2. Consolidation et promotion de l’arabité et de l’amazighité

Soucieux de renforcer le substratum de notre culture et le tissu de l'identité de notre nation riche
parla diversité de sesaffluents, le gouvernement entend promouvoir et consolider la langue arabe
et tamazight à travers notammentla généralisation de l’utilisation de la langue arabe dans les
domaines scientifique et technologique et la consolidation et le renforcement de l'usage de
tamazight dans le système éducatif.
                                                 18
Il'entreprend également de donner corps au caractère officiel de la langue amazighe au travers de
mesures visantl'intégration de cette dernière dans les domaines prioritaires de la vie publique,
selon une démarchede partenariat avec l'ensemble des acteurs dans la sphère de promotion dela
langue et de la culture amazighes, qui seront associés dans l’entreprise que le gouvernement
compteinitier dans une approche scientifique et empreinte de sérénité visant la réappropriation
desréférents culturels etlinguistiques amazighe à travers le rétablissement de leurs toponymies
ancestrales, notamment les communes et localités qui retrouveront leur dénomination originelle.

Il veillera à promouvoir la culture amazighe dans toutes ses expressions et lui assurera son
rayonnement dans l'espace social, culturel et médiatique national.

6.3. Préservation de la mémoire nationale

Le gouvernement veillera à prendre toutes les mesures et les dispositions réglementaires,
juridiques et matérielles qui visent à préserver la mémoire nationale et à garantir une prise en
charge optimale de l'honorable catégorie des moudjahidine et des ayants droits.

Il veillera à faire aboutir les dossiers relatifs aux disparus de la guerre delibération nationale,
l'indemnisation des victimes des essais et explosions nucléaires, la récupération des archives
nationaleset la restitution du patrimoine national spolié durantla période coloniale.

Il s'emploiera à promouvoir l'enseignement del'histoire et sa transmission aux jeunes générations
et à intensifier les actions de collecte et d'enregistrement des témoignages, en assurant leur
classementetleur exploitation.

7. Une relation complémentaire entre les pouvoirs exécutif etlégislatif

Le gouvernement œuvrera pourl'amélioration etle renforcementdesrelations fonctionnelles avec
les deux chambres du parlement et pour intensifier les efforts visant à promouvoir la pratique
parlementaire en contribuant au renforcement du rôle du parlement et ce dansle strict respect du
principe de séparation des pouvoirs et les dispositions constitutionnelles, afin d'affirmerl'ordre
démocratique fondésurle pluralisme politique.

Le gouvernementinscrit parmi ses priorités l'engagementtotal et entier de ses membres à assister
aux travaux parlementaires et de répondre aux questions orales et écrites des membres du
parlement dans les délais fixés par la Constitution. Il s'engage également à présenter
annuellement la déclaration de politique générale.

Le gouvemement œuvrera à étudier toute proposition de loi émanant des membres du parlement,
et affirmera son entière disponibilité à examiner l'ordre du jour proposé par l'opposition
parlementaire, conformément aux dispositions constitutionnelles.

Lesliens entre les deux institutions exécutive etlégislative doivent être caractérisés par le respect
et la confiance mutuelle, à travers l'engagement des membres du gouvernement à recevoir les
parlementaires, afin d'être à l'écoute des préoccupations des citoyens de leur circonscription
électorale, en vue de leurs prise en charge et c'est aussi l'engagement auquel doivent souscrire
les autorités locales, ce quitraduit le respect del'autorité exécutive envers l'autorité législative.




                                                 19
Dansce cadre, le gouvernement s'engage à assurerle suivi de la prise en charge des problèmes
soulevés par les parlementaires et à respecter et coopérer pleinementlors de la mise en œuvre
des mécanismes de contrôle notamment les commissions d'enquête et les missions temporaires
d'information.

La démarche que le gouvernement compte adopter vise à renforcer l'esprit du dialogue, de
concertation et de complémentarité fonctionnelle qui devrait distinguer la relation entre le
gouvernement et le parlement ce qui contribuera à accélérer le processus de développement
national et à améliorer le cadre de vie descitoyens.

Enfin le gouvernementveillera à concrétiser, avec le concours du parlement, le projet de création
d'une chaine de télévision parlementaire et ce, dans le cadre global de la démarche visant à
accroitre la transparence del'action publique.




                                               20
                                      CHAPITRE DEUXIEME
           POUR UNE REFORME FINANCIERE ET UN RENOUVEAU ECONOMIQUE


1. Acterla réforme financière

Le plan d'action du gouvernement ambitionne de réformer en profondeur le système financier
national dans un contexte particulier caractérisé par:

- la fragilisation des équilibres budgétaires et de trésoreries, en raison d'un net recul des recettes
  fiscales, dû principalementà la chute tendancielle des cours pétroliers ;
- la régression de la fiscalité pétrolière dont les recettes sont passées de 4054,349 Mrds DA en
  2012 à 2 666,9 Mrds DA en 2019 ;
- la faiblesse du niveau dela fiscalité ordinaire qui peine à couvrir le budget de fonctionnement
   del'Etat;
- la contraction de nos capacités financières face à une demande intere croissante aussi bien
   en matière d'investissement que de consommation ;
- un réseau bancaire public peu performant, qui représente 90% des actifs bancaires, orienté
   versle financement desinfrastructures, pour l'essentiel issues de la commandepublique:
-_ l'interventionnisme del'Etat en faveur desentreprises publiques défaillantes;
- l'absence d'indications précises sur le coût budgétaire implicite engendré par les multiples
   mesuresincitatives accordées.

Partant, la réforme projetée portera surl'application des principes de bonne gouvernance fiscale,
budgétaire et financière et reposera sur la gestion par la performance, la modernisation et
l'informatisation des services, la transparence et la maitrise desrisques, qui constitueront la pierre
angulaire du système financier national.

1.1 Refonte du système fiscal

Une nouvelle politique fiscale à court, moyen et long termes sera mise en œuvre par le
gouvemementvisant à garantir la compétitivité de l'économie nationale, le financement adéquat
de l’action publique et la justice sociale et veillera, sur un autre plan, à assurer une meilleure
cohérence et prévisibilité de notre système fiscal ; l'objectif étant l'augmentation des recettes
fiscales à traversl'expansion de l'activité économique et non parla hausse du niveau des impôts.

Dans ce cadre, le gouvernement s'engage à mener un combat sans merci contre la fraudefiscale,
en appliquant de manière plus efficace les sanctions prévues par la loi qui seront durcies en
conséquence.

Par ailleurs, il initera les mesures appropriées afin d'améliorer sensiblementl'efficacité de la
perception des impôts et d'en réduire les coûts. || outillera à ce titre l'administration fiscale de
manière à permettre le traitementet le paiementà distance des déclarations d'impôts et dotera ses
structures en moyens nécessaires pour procéder à des simulations et des études d'impact sur
toutes les dispositions projetées.




                                                 21
Outre la simplification des procéduresfiscales, le gouvernementveillera à améliorerl'organisation,
la gestion et le fonctionnement des services fiscaux par un effort accru de formation et
d'optimisation des ressources humaines en vue d'assurer un meilleur contrôle fiscal des assujettis.

L'amélioration de la gouvernancefinancière se concrétisera, notamment, par le confortement de
l'éthique aussi bien pourle secteur public que privé, à travers le renforcement de la prévention et
de la lutte contre la fraude financière sous toutes ses formes. Une attention particulière sera
accordée au renforcement du contrôle inteme del'administration fiscale et douanière.

Pour lutter contre les fléaux financiers, tous les moyens de l'Etat seront mobilisés en vue
d'intensifier la lutte contre les fausses déclarations, la fraude fiscale ainsi que les infractions
douanières, particulièrement en matière de surfacturation et de fuite de capitaux.

Dans ce cadre, les organes de contrôle, notamment la Cour des Compteset l'inspection générale
des finances, verrontleurs attributions élargies et leurs moyens renforcés pour lutter contre ces
fléaux. Un organisme multisectoriel sera également mis en place en vue de lutter efficacement
contrela fraudefiscale et financière.

La réforme envisagée devra répondre auxcritères de prévisibilité, de stabilité et de compétitivité en
matière fiscale, d'une part et de pérennisation des emplois, d'équité sociale, d'autre part, afin
d'assurer une contribution équitable des différentes catégories de contribuables et de revenus au
financement des dépenses publiques.

En particulier, le gouvernement procédera à la révision des niveaux de l'impôt surle revenu global
des salariés, en les adaptant selon les différentes tranches de revenus dans une démarche
d'équité sociale. Dans ce cadre,les revenus mensuels inférieurs à 30.000 dinars seront exonérés
d'impôts.

Parailleurs, et afin d'encouragerle travail à domicile, une défiscalisation des revenus de ce type
d'activités sera opérée. Dans le même sillage, les activités artisanales bénéficieront d'avantages
fiscaux appréciables. Globalement, tout projet de réforme fiscale devra encourager l'emploi et
veillera à l'amélioration du pouvoir d'achat des bas revenus.

Afin d'améliorer la compétitivité et l'attraction de l'économie algérienne aux Investissements
Directs Etrangers (IDE), le gouvernement prendra une série de mesures destinées à rassurer les
investisseurs étrangers potentiels. Pour ce faire, de nouvelles règles de gouvernance seront
introduites dans tous les secteurs del'économie et seront basées sur:
-_ la mise en place d’une procédureclaire et transparente surle transfert des dividendes conforme
   aux principes et aux règles internationales;
-_ la modernisation du régime fiscal dela propriété intellectuelle ;
-_ la modernisation du régime applicable aux relations entreprise mère-filiales;
- la stabilité du dispositif juridique régissantle régime fiscal appliqué à l'investissement.
-_ Concernant la fiscalité directe, le gouvernementinstaurera une procédure uniforme en matière
   de décisions fiscales dans un but de transparence, de cohérenceet de sécurité juridique pour
   les opérateurs.

Aussi, la simplification du système fiscal qui sera au cœur de la réforme envisagée se traduira, à
court terme, parla suppression des taxes à faible rendement, d'une partet, d'autre part, la révision
du système des avantagesfiscaux et parafiscaux ayant engendré, parle passé, de gravesdérives.

                                                 22
En parallèle, le gouvemements’attèlera à la création de nouveaux impôts liés au capital et la
richesse,tout en veillant à préserverl'outil de production créateur d'emploi et de valeur ajoutée et
soutiendra les investissements permettant un développement et une croissance économique
soutenue, notamment pourles PME et les Start-up.

Le gouvernement mettra aussi en place un mécanisme d'abattementfiscal afin d'encourager les
entreprises à renforcer le financement par fonds propres. Il renforcera parailleurs l'encadrement
dela profession du conseil fiscal pour conforterles professionnels actifs dans ce domaine.

Concernantla fiscalité locale,la réforme portera surla diversification des recettesfiscales locales
afin de doterles collectivités locales de ressourcesplus significatives et de renforcerla redevabilité
entre citoyens et collectivités parl'offre au niveau local de services adéquats. Dans ce cadre,il
sera procédé à court terme à l'annulation de la taxe surl'activité professionnelle ; parallèlement
l'impôt foncier sera révisé en profondeur dans le cadre de la réforme envisagée, qui redéfinira le
financement descollectivitésterritoriales sans compromettre leurviabilité.

1.2 Instauration de nouvelles règles de gouvernance budgétaire

La nouvelle dynamique économique que le gouvernement compte impulser sera accompagnée
d'unepolitique budgétaire rénovée, basée surla rationalisation de la dépense publique et visant à
lui conférerun effet delevierstimulant del’activité économique.

Parmi les actionsprioritaires à engager dans ce cadrefigurele rétablissement dela discipline et la
rigueur budgétaires qui se traduiront, à terme, par la consécration de la performance de la
dépense et son adéquation avecles ressourcesfinancières disponibles.

L'efficacité de la dépense publique sera assurée par l'instauration d'un procédé de suivi et
d'évaluation économique de toute mesure sollicitant le budget de l'Etat, à la faveur de la
généralisation du système d'information et de gestion budgétaire à tous les départements
ministériels ; ce qui confortera la mise en place du cadre de dépense à moyen terme et du cadre
budgétaire à moyen terme, deux outils indispensables pour l'élaboration d'une trajectoire
budgétaire pluriannuelle.

L'efficacité de la dépense publique sera également assurée par la mise en œuvre d'une réforme
graduelle et globale des subventions, dont le niveau avoisine actuellement 25 % du PIB, parla
mise en place d'un système de ciblage des populationsles plus vulnérables.

S'agissant des équilibres de comptes publics, qui englobent le solde budgétaire et le déficit des
caisses de sécurité sociales et de retraites, l'objectif du gouvernement à travers les réformes
projetées consiste à ce que les recettes dela fiscalité ordinaire à elles seules puissentfinancer, à
l'horizon 2024, la totalité du budget de fonctionnement.

Il convient de souligner, à titre d'illustration, que les recettes collectées au titre de la fiscalité
ordinaire en 2019, qui s'élèvent à 3110 Mrds DA, ne représentent que 64% des dépenses de
fonctionnement.

1.3 Modernisation du système bancaire etfinancier

Le secteur bancaire et financier connaitra une réforme importante en vue de le moderniser et de
diversifier les produits financiers. L'accent sera mis principalement sur une large diffusion auprès
de la clientèle des banques des instruments de paiement électronique, en vue de réduire les
transactions en « espèces».

                                                  23
Aussi, l’année 2020 verra la création de banques spécialisées et de fonds d'investissement dédiés
respectivement à l'habitat et aux petites et moyennes entreprises et start-up, ainsi que le
déploiement de certaines banques nationales à l'étranger, à travers l'ouverture d'agences.

Lesinstitutions bancaires et d'assurances seront encouragées à diversifier l'offre de financement
par la dynamisation du marché du crédit, la généralisation des produits financiers et le
développement du marché obligataire.

Elles serontincitées à déployer les moyens nécessaires pour une large inclusion financière afin
d'encouragerl'épargne et d'assécherlesliquidités prospérant hors circuit bancaire et les canaliser
versles activités formelles.

Ainsi, les institutions bancaires et d'assurances seront soutenues pour encouragerl'innovation
financière,la distribution de produits financiers novateurs, y compris la mise sur le marché de titres
dans le cadre du financementalternatif comme les « soukouk » et développer les réseaux du
paiement électronique.

Parailleurs, les banques seront appelées à améliorer, sous l'autorité et le contrôle de la Banque
d'Algérie,le suivi et le recouvrement de leurs créances.

Enfin une attention particulière sera portée au développement de la bourse, appelée à jouer un
rôle essentiel dans le financement de l'entreprise ainsi qu'à la dynamisation des marchés de
capitaux améliorant ainsi l'efficacité globale du système financier et l'allocation des ressources
financières.

1.4 Développementdel'information statistique et de la fonction prospective

L'efficacité de l'action économique de l'Etatesttributaire de ses capacités de prévision qui, elles-
mêmes, doivent pouvoir compter sur un outil statistique puissant et fiable, qui bénéficiera d'un
renforcement substantiel en moyens humains et matériels.

Dans ce cadre,la réalisation prochaine du Recensement Général des Populations et de l'Habitat
constituera un jalon important apportant une meilleure visibilité, en appui des travaux sur les
perspectives del'évolution de l'économie algérienne etla réalisation dela vision « Algérie 2035 ».

La fonction prospective et statistiques est appelée à jouer un rôle essentiel dans l'élaboration des
différentes stratégies sectorielles, compte tenu de son caractère transversal.

Les actions à mener pourrenforcerles capacités del'Etat et de ses démembrements en matière
d'outils d'aide à la décision consisteront dans:

-_ la finalisation de l'étude stratégiqueintitulée « Vision Algérie 2035 » destinée à servir de cadre
   référentiel fondamental pourles différents secteurs ;
- le développement d'un système national d'information statistique, économique et sociale,
  intégrant les normes de transparence,d'actualisation et d'authentification des données et leur
  fluidité au profit des agents économiqueset desinstitutions del'Etat.




                                                  24
2. Impulser le renouveau économique

Les actions engagées dans les domaines économiques ainsi qu'en matière de promotion de
l'investissement dans notre pays, aussi bien par l'Etat que par les entreprises, n'ont pas eu les
effets escomptés surles performances del'économie dans son ensemble.

Lesfaibles performances enregistrées dansle secteur industriel et minieret le caractère erratique
de la croissance économique dans notre pays sont loin de refléter les besoins lancinants de
développement économique, territorial et technologique et d'intégrer les nouveaux enjeux de
compétitivité et de transformation de l'économie, notamment ceux liés au développement
vertigineux de l'économie numérique.

Tenant compte de ces besoins et de ces enjeux, qui justifient une attention prioritaire des pouvoirs
publics, et pour faire face aux défis de la refondation de l'économie nationale, le gouvernement
adoptera une nouvelle politique économique articulée autour des grandes orientations stratégiques
suivantes:

- structurer l'économie autour des secteurs pourvoyeurs d'emplois, porteurs d'intégration et
   valorisant en priorité toutes les ressources du pays, tournés à terme versl'exportation ;
-_ créer un environnement desaffaires transparent et équitable, favorable à l'investissement et à
   l'entrepreneuriat;
- mettre en place un nouveau mode de gouvernance économique et de management de
   l'entreprise;
- faire émerger une nouvelle économie fondée sur l'innovation, la compétitivité, la qualité et le
   savoir.

La refondation de l'économie nationale nécessite dès lors des actions vigoureuses de
redressement autour d'un nouveau modèle de développement économique centré sur la
valorisation des ressources nationales et du potentiel humain.

2.1 Promotion du cadre de développement de l’entreprise

Les entreprises constituent un patrimoine économique important du pays et recèlent d'importantes
capacités productives. Le gouvernement entend, à ce titre, poursuivre et dynamiser les
programmes de mise à niveau des PME dans tous les secteurs d'activité.

Soucieux de faire évoluer et effacer toutes discriminations factices ayant caractérisé les relations
entre les entreprises privées et publiques, le gouvernement œuvrera à adopter une doctrine
économique de développementde l'entreprise algérienne sans distinction entre entités publiques
et privées.

Dans ce cadre, le partenariat entre entreprises publiques et privées, sera encouragé dans un
cadre organisé, dans l'objectif de rehausser la compétitivité et de maximiser l'utilisation des
capacités de production disponibles dans tous les secteurs d'activité.

Il'encouragera égalementla promotion des partenariats avecles investisseurs étrangers dans tous
les domaines, dans un cadrejuridique rénové.




                                                25
Le gouvernementveillera à développer les capacités nationales en matière de normes et de
laboratoires de contrôles industriels, dans une perspective d'amélioration de la qualité de la
production industrielle et de la compétitivité.

Aussi, il sera question de renforcer le secteur public marchand et de lui assurer les chances de
succès, en effectuant une refonte de l'environnement juridique qui encadre la gestion des
participations de l'Etat, de l'entreprise publique et du partenariat pour l'adapter aux nouvelles
exigences que commandela politique de renouveau économique, acté par le président de la
République.

Dans cecadre, il sera procédé aux amendements nécessaires qui consacrent l'autonomie effective
de la décision de l'entreprise publique et uneflexibilité accrue de son fonctionnementet favorisent
un cadre souple encourageant le partenariat, sans pour autant affecter l'efficacité de la
supervision.

Au titre de l'accompagnement des entreprises, le gouvernements’attachera à mettre en place un
dispositif approprié d'assistance ou d'aide aux PME en difficulté, permettant l'assainissement des
créances détenues parles opérateurs économiques sur les démembrements del'Etat.

2.2 Amélioration substantielle du climat desaffaires

Le développement économique représente un déf et un enjeu majeurs et devra s'inscrire dans une
démarche globale d'amélioration de la compétitivité de l'économie nationale, en général, et de
l’entreprise en particulier, afin de la consolider et de la maintenir dans une dynamique de véritable
croissance,à traversl'émergence d'un environnementattractif.

À cetitre, le gouvernement veillera à rénover et stabiliser le cadre juridique de l'investissement,
afin d'encouragerl'acte d'investir et attirer davantage les IDE. || œuvrera égalementà :

-_ mettre fin aux entraves et pesanteurs bureaucratiques qui persistent surle terrain au détriment
   dela dynamique del'investissement, en développant les processus de contrôle à posteriori;
-_ simplifier et réduire le champ des autorisations liées à l'investissement en définissant, dans la
   transparence, lescritères d'éligibilité aux avantages des projets d'investissement.
-_ évaluerles différents avantages édictés par le code des investissements et veiller à s'assurer
   quela charge fiscale supportée parl'Etat se traduise par un développement socio-économique,
   unecréation d'emplois, des recettesfiscales additionnelles et uneparticipation au redressement
   de la balance des paiements ;
- mettre en place une grille d'évaluation et de modulation des avantages accordés aux projets
   d'investissement;
- maintenir les régimes préférentiels d'encouragement à l'investissement prévus au bénéfice des
  projets qui seront réalisés dans les wilayas des Hauts Plateaux et du Sud.

2.3 Rationalisation du déploiement territorial, du développement industriel et de
l’exploitation du foncier économique

La réponse à la demande importante en foncier industriel implique une rationalisation et une
densification du déploiement des activités productives, à travers tout le territoire national, pour
garantir une propagation du développement économique dans toutesles régions du pays.



                                                 26
Pour cefaire, le gouvernementveillera à lutter contre la littoralisation des activités économiques en
élaborant une carte nationale des opportunités d'investissements et en ouvrant de nouveaux
espacesde production du foncier économique, notamment dans les Hauts-plateaux et le Sud du
pays.

En outre,le gouvernement œuvrera à :

-   améliorerla gestion du foncier industriel, en accentuantla lutte contreleslots en déshérence;
- diligenter la mise en service de nouvelles zones industrielles en priorisant la réalisation de
  celles implantées dans des zones enregistrant un important déficit en matière de foncier;
- aménager de nouvelles zones d'activités économiquesetles doter des commodités requises;
- impliquerla réhabilitation des opérateurs économiques des zones industrielles en activité.

En tout état de cause, la réalisation etla gestion des zones industrielles fera l'objet d'une approche
nouvelle, à travers la mise en place d'un nouveau cadreinstitutionnel et juridique redéfinissantles
rôles de tous les intervenants. Dans ce cadre, le gouvernementveillera à réprimer sévèrement les
tentatives de détournement du patrimoine foncier économique de sa vocation ou d'immobilisation
indue sansréaliser les investissements annoncés.

2.4 Développementstratégique desfilières industrielles et des mines

Il s'agira de mettre en place un cadre juridique cohérent de promotion de l'investissement
productif, notamment dans:

-   les industries agro-alimentaires;
-_ l'électronique et l'électroménager;
-   les industries de matériaux de construction ;
-   les industries mécaniques;
-_ l'industrie pharmaceutique ;
-   les industries chimiques;
- les industries textiles et cuir.

Un effort accru sera plus particulièrement investi pour développerles industries de transformation
de matières premières, notamment agroalimentaires, sidérurgiques et en aval des hydrocarbures,
avec le concours des investisseurs nationaux et étrangers. Cet effort qui dégagera une plus-value
dans la valorisation locale des ressources agricoles, minières et minérales, permettra le
développement d'autres branches de l'industrie locale.

Par ailleurs, le gouvemement veillera à réviser les textes réglementaires encadrant le dispositif
CKD, dansl'objectif d'augmenter le taux d'intégration nationale dans les activités de montage et
d'assemblage, notamment dans les industries mécaniques, électriques, électroniques et de
promouvoir lesfilières de sous-traitance dans ces domainesd'activité.

A cet effet, les investissements dans les processus industriels des activités de sous-traitance
bénéficieront d'un cadre incitatif d'accès au foncier et au financement et d'un régime fiscal
préférentiel. Les autres produits éligibles à une intégration nationale seront égalementidentifiés et
bénéficieront de mécanismes et outils pourleur développementet leur promotion.




                                                    27
Le gouvernement veillera à mettre en place les conditions requises pour la Valorisation du
patrimoine économique minier dont dispose le pays. Des programmesd'explorations et d'études
des potentialités minières seront conduits dans toutes les régions du pays, parallèlement à la
concrétisation des grands projets en maturation, notamment pour :

-_ valoriser le potentiel de phosphateset la production des différents types d'engrais, à l'effet de
    satisfaire le marché nationaletd'alimenterles exportations;
-   densifier la production nationale de fer, d'or, de plomb et zinc, de marbre et de pierres
    décoratives;
-_ faire progresserles études pour l'exploitation des gisements de minerais de fer de Gara Djebilet
    et de Mecheri Abdelaziz dans la wilaya de Tindouf;
-_ développeret exploiter le gisement Plomb-zinc d'Oued Amizour/Bejaïa ;
- mettre en place des programmes de développement adaptés aux zones frontalières et aux
   zones éloignées à fort potentiel minier et aurifère, en favorisantl'exploitation artisanale de l'or
   dansla région du Hoggar/Tamanrasset-Illizi et la valorisation de filons aurifères identifiés.

Sur un autre plan, le gouvernement veillera à :

-_ diversifier les sources de financement des activités minières en élargissant la contribution du
   secteurprivé national et étranger ;
- encouragerle recours au partenariat, notamment technologique, pourl'exploitation des activités
    minières;
- mettre en place une agence de contrôle réglementaire pour une meilleure gestion des activités
    minières.

2.5 Renforcementdes capacités institutionnelles en matière de développementindustriel et
minier

Il s'agira de réaliser un état des lieux approfondi des filières industrielles sur la base d'une
démarche et de diagnostic stratégique par une approche en termes de systèmes et filières afin
d'identifier les goulots d'étranglement du développement industriel à l'amont de ces filières et les
entraves persistantes à l'ouverture sur les marchés extérieurs. Il s'agira également d'évaluer la
maturité technologique, organisationnelle et numérique des entreprises industrielles et reconfigurer
les instruments de financement public del'industrie, des mineset del'innovation.

Le gouvernement mettra en place un cadre de concertation nationale pour le développement
industriel et minier, en réhabilitant notamment le Conseil national consultatif pour les PME, et
œuvrera à sensibiliser tous les acteurs aux enjeux et défis et à créer un réelclimat de confiance.

2.6 Valorisation de la production nationale

La promotion de la production nationale repose sur une exigence vitale qui consiste à rétablir, sur
des bases pérennes, l'équilibre de la balance des paiements par la réduction des importations
improductives et la promotion des exportations hors hydrocarbures.

A cet égard, le gouvernement mettra en place des dispositifs juridiques obligeant les opérateurs
publics et privés et l'ensemble des gestionnaires et ordonnateurs du budget public à recourir aux
biens et aux services produits localement dans le cadre de leurs commandes. Ces obligations
seront notamment matérialisées dansles cahiers des chargesrelatifs aux marchés publics.

Le gouvernement poursuivra une démarche volontariste de régulation et de réduction des
importations, en protégeantles produits dont la demande est satisfaite parla production locale.
                                                  28
2.7 Rationalisation des importations et promotion des exportations
La politique commerciale nationale a connu jusque-là un dysfonctionnement dans l'encadrement
du commerce extérieur, une défaillance imputable au déficit chronique de la balance commerciale
du fait de la hausse considérable de la facture des importations et d'une faible contribution des
exportations hors hydrocarbures.
Afin de redressercette situation grandementpréjudiciable à l'économie nationale, l'intervention du
gouvemement sera orientée, en priorité, sur la mise en place urgente d'une nouvelle politique
commerciale, qui s'articulera autour de deux principaux axes, à savoir la promotion des
exportations et la rationalisation des importations, sans pour autant impacter la satisfaction des
besoins des citoyens.

Cette nouvelle politique vise également à substituer les importations par une production
nationale diversifiée, à promouvoir l'exportation, à éradiquer le phénomènede la surfacturation à
l'importation et à assurer enfin l'équilibre de la balance des paiements.
Parailleurs, le gouvernements'engage fermement à encourager et à accompagnerla promotion
des exportations hors hydrocarbures, en apportant les appuis nécessaires aux opérateurs
économiques dans l'amélioration de leur compétitivité et la création, en tant que de besoin, des
consortiums d'exportation.
La promotion des exportations doit se traduire par des mesures de mise à niveau des différents
texteslégislatifs et réglementaires régissant et encadrant l'acte del'export y compris le troc, basée
sur une approchede simplification et d'amélioration des procédures,
La réalisation de zones franches au niveau des wilayas frontalières du Sud, la création de
plateformeslogistiques dédiées à l'exportation, la mise en place de mesuresincitatives inhérentes
aux zones économiques spéciales et la création de grands centres d'expositions, sera
accompagnée par la consolidation du cadre de soutien à l'exportation, notamment la relance des
activités du Conseil national consultatif pour la promotion des exportations et le renforcement du
rôle dela diplomatie économique au service des entreprises exportatrices.

Parailleurs, le gouvernement envisage d'engager une profonde évaluation et révision des accords
delibre-échange existants. À cetitre, il sera procédé à :
- la définition descritères pourla conclusion de nouveaux accords commerciaux préférentiels ;
- l'évaluation des accords multilatéraux et bilatéraux (accord d'association avec l'Union
  européenne,grande zone arabedelibre échange, accord préférentiel avecla Tunisie) ;
-   la finalisation de l'adhésion del'Algérie à la ZLECAF;
-_ l'évaluation du processus d'accession del'Algérie à l'OMC;
-_ la mise en œuvre de la stratégie nationale des exportations.
2.8 Assainissement de la sphère commerciale
Le gouvernement envisage d’axer sesefforts dans ce domaine au titre des cinq (05) prochaines
années autour de deux axes fondamentaux à savoir :
L'organisation et l'encadrement du commerceintérieur, dont les principaux objectifs consistent
en la lutte contre le commerceinformel, l'assainissement du marché et la moralisation del'acte de
commerce, la densification des équipements commerciaux, la numérisation du secteur
commercial, le développement du e-commerce,la révision des cadreslégislatifs et réglementaires
relatifs aux activités commerciales et le renforcement du dialogue et de la concertation avec les
partenaires sociaux et professionnels.
                                                  29
Dansce cadre, le gouvernement a retenu les mesuresprioritaires suivantes:

- la densification de la campagnenationale delutte contre le gaspillage ;
- l'absorption du commerce informel et l'intégration des intervenants dans le tissu commercial
   légal ;
- la mise en place d'un programme d'urgence portant sur l'exploitation de 625 marchés couverts
   et de proximité inexploités, l'achèvement de la réalisation des marchés de gros à vocation
   régionale ainsi que le développement dela grande distribution.
-_ l'élaboration d'un schéma directeur nationald'implantation des équipements commerciaux ;
- la numérisation des opérations, l'interconnexion avec les secteurs impliqués dans l'acte
    commercial et la généralisation de l’'e-Paiement;
- la refonte du dispositif relatif au remboursement des frais de transport des Wilayas du Sud.

La modernisation et le renforcement del'outil de contrôle dont les objectifs porteront sur la
révision de la stratégie globale du contrôle économique et de la répression des fraudes, le
renforcement des capacités analytiques du secteur du commerce, la dématérialisation des
procédures de contrôle et le renforcement des mécanismes de protection de la santé, de la
sécurité et desintérêts du consommateur. A cetitre, les actions suivantes seront lancées:

-_ l'élaboration d'une banque de donnéesde la production nationale (produits agricoles, industriels
    et services) ;
- le parachèvementdel'opération relative au registre de commerce électronique ;
-_ le renforcement du respect des normes et des spécifications légales et réglementaires régissant
    les produits et les services ;
- la révision de la stratégie nationale du contrôle économique et de la répression des
   fraudes, notammentdans ses voletsrelatifs aux sanctions applicables aux contrevenants ;
- la numérisation des procédures de contrôle du marché domestiqueet aux frontières;
-_ le renforcement des capacités analytiques des 45 laboratoires d'analyse;
-   le renforcement du rôle du Laboratoire National d'Essai :
- la mise en place d'un réseau national de laboratoires dans les ports et aéroports pour le
  contrôle de qualité des produits alimentaires et autres. Dans ce cadre, une Agence de Sécurité
    Sanitaire des Aliments sera créée ;
- la mise en place d'un plan de formation visant à assurer une plus grande technicité aux agents
  chargés du contrôle.

2.9 Transition énergétique

La transition énergétique occupe une place importante dans le programme du gouvernement qui
vise, outre l'intensification des efforts de prospection et de production des hydrocarbures, la
diversification des sources énergétiques à travers le développement des énergies renouvelables et
la promotion del'efficacité énergétique.

Cette transition devrait permettre à notre pays de s'affranchir de manière progressive de la
dépendancevis-à-vis des ressources conventionnelles et d'amorcer une dynamique d'émergence
d'une énergie verte et durable qui s'appuie sur la mise en valeur de ressources d'énergie
inépuisables.




                                                  30
Cette démarches'articule surles considérations suivantes:

-   la préservation des ressourcesfossiles et leur valorisation:
- le changement du modèle énergétique de production et de consommation;
- le développement durable et la protection del'environnement ;
- la maitrise des coûts de réalisation desinstallations des énergies renouvelables.

Autitre du développement des énergies renouvelables

Tenant compte du potentiel existant et des capacités d'absorption de notre réseau national de
transport et de distribution de l'énergie électrique et de l'énergie d'origine renouvelable, un
programme adapté de développement des énergies renouvelables d'une capacité de 15.000 MW à
l'horizon 2035, dont 4000 MW d'ici 2024, sera mis en œuvre.

La réalisation de ces capacités permettra non seulement d'économiserprès de 240 milliards de m3
de gaz naturel et d'éviter ainsi l'émission de 200 millions de tonnes de CO, mais aussi le
développement effectif d’un tissu de PME surl'ensemble de la chaine de valeur des composants
dédiés aux énergies renouvelables.

L'hybridation de la production del'électricité de source conventionnelle au niveau du Sud du pays,
par la production photovoltaïque, constitue pour le gouvernement une action prioritaire dans ce
domaine.

Aussi, la mise à niveau de la réglementation d'encouragement de la production d'électricité à partir
de sources renouvelables afin d'y intégrer des mécanismes adaptés à l'auto production par les
résidentiels, sera rapidement mise en œuvre.

Autitre dela politique d'efficacité énergétique

Les mesuresfortes que le gouvernement envisage de mettre en œuvre en matière d'efficacité
énergétique permettront de réduire drastiquement le gaspillage et de préserver les ressources
énergétiques du pays.

Cette démarche, qui sera encouragée dans les différents secteurs d'activité, sera axée surles
mesures suivantes:

- la généralisation des procédésd'isolation thermiques dans les nouvelles constructions ;
- la mise en place d'un programme national pour la conversion des véhicules au GPL c et le
  développement du GNC pour les véhicules de transport collectif ;
- l'équipement du réseau d'éclairage public et des administrations publiques avec des dispositifs
    à basse consommation ;
- la mise en place d'un cadre réglementaire prohibant l'importation et la production
   d'équipements énergivores;
-_ l'élargissement du dispositif incitatif à l'investissement auxfilières permettantla localisation de
   l'activité de production d'équipements et de composants dédiés l'efficacité énergétique.

Ces mesures constituent une réponse appropriée au défi de conservation del'énergie avec toutes
ses implications bénéfiques sur l'économie nationale en termes de création d'emplois et de
richesses, en plus de la préservation del'environnement.


                                                  31
En matière d'hydrocarbures

Parallèlement aux efforts de diversification de l'économie nationale dans le cadre du projet de
renouveau économique, le gouvernement s’attèlera, dans le domaine des hydrocarbures, à
assurer la couverture des besoins nationaux, la sécurité de l’approvisionnement ainsi que le
pourvoi de l'économie nationale en ressourcesfinancières. Ces impératifs passent par:

-_ l'intensification del'effort de rechercheet d'exploration, y compris dans les zones Offshore etle
   nord du pays, pour mettre en évidence de nouvelles réserves d'hydrocarbures ;
- l'optimisation de l'exploitation des gisements d'hydrocarbures parl’utilisation de méthodes de
   récupération assistée tout en garantissant la conservation des gisements :
- le renforcement des capacités de production.

Parailleurs, un programmede valorisation des hydrocarbures est retenu par le gouvernement,afin
d'asseoir uneindustrie pétrochimique et de raffinage etce, à travers :

-_ le lancement de projets intégrés à forte valeur ajoutée, tels que le vapocraquage d'éthane pour
   la fabrication des polymères et produits dérivés;
-_ le développement d'une industrie de transformation des phosphates;
- l'augmentation des capacités de raffinage pour satisfaire la demande nationale en produits
   pétroliers.

Parailleurs, l'approvisionnement continu du marché national en produits pétroliers sera assuré à
travers l'augmentation des capacités de stockage de carburants à 30 jours contre 12 jours
actuellement.

S'agissant des hydrocarbures non conventionnels, le gouvernement,tout en intensifiant les efforts
d'identification du potentiel que recèle notre sous-sol, engagera les études appropriées sur l'impact
del'exploitation de cette richesse au plan économique, social et environnemental, en veillant à ce
que toute exploitation envisagée préserve la santé du citoyen, les écosystèmes et, en particulier,
les ressources hydriques.

2.10 Agriculture et pêche modernes, pour une meilleure sécurité alimentaire

Autitre del’agriculture

Le gouvernement ambitionne de mettre en œuvre une politique agricole durable permettant de
renforcerla sécurité alimentaire du pays, de réduire le déséquilibre de la balance commerciale des
produits agroalimentaires de base et de contribuerefficacement à la diversification de l'économie
nationale.

Lesefforts de développementdel'agriculture et desterritoires ruraux et del'agriculture saharienne
et des montagnes, seront orientés vers la mise en œuvre d'actions structurées, visant une
occupation harmonieuse des territoires ruraux, conjuguée à une exploitation durable des
ressources naturelles en favorisantl'initiative privée créatrice de la richesse et de l'emploi.

La protection et la valorisation des espaces naturels en généralet le patrimoine forestier et alfatier
en particulier, permettront d'assurer une production accrue de biens et services au profit des
populationslocales et del'économie nationale.

                                                 32
Aussi, à l'horizon 2024, le gouvernement ambitionne d'atteindreles objectifs suivants :

   la couverture des besoins alimentaires de base par l'accroissement de la production et de la
   productivité agricoles;
   le développement d'un nouveau modèle agricole et rural, porté parl'investissement privé et
   l'émergence d'une nouvelle génération de producteurs ;
   l'amélioration de la compétitivité des produits agroalimentaires et forestiers et l'intégration des
   chaines de valeur à l'international ;
   le développement durable et équilibré des territoires ruraux, notamment dans les espaces de
   montagnes et du Sahara;
   la modernisation de l'agriculture par l'intégration des produits de la connaissance et de la
   numérisation.

Pour atteindre ces objectifs, les actions suivantes seront initiées:

   le développement de la production agricole à travers l'extension de la superficie irriguée et
   généralisation de l'utilisation des systèmes économes en eau et des énergies renouvelables
   dansl'agriculture ;
   l'accroissement de la production et de la productivité agricoles à travers (i) l'utilisation des
   techniques modernes dans le monde agricole, (ii) la modernisation des programmes de
   production de semences, plants et géniteurs et le renforcement des systèmes deveille sanitaire
   et phytosanitaire,(iii) la promotion des produits agricoles et forestiers pour lesquelles l'Algérie
   dispose d'avantages comparatifs, pouvant permettre l'accroissement des exportations (iv) et le
   renforcement du système de certification et de labellisation des produits etla rationalisation de
   l'importation des produits alimentaires subventionnés;
   l'exploitation rationnelle du foncier agricole à travers le renforcement du dispositif juridique
   permettant la préservation et la protection des terres agricoles, à travers (i) l'assainissement et
   la récupération des terres non exploitées et leur réaffectation au profit des investissements
   agro-industriels et des jeunes porteurs de projets et(ii) la facilitation du lancementeffectif des
   projets d'investissements pour la mise en valeur des terres dans le Sahara et les Hauts
   Plateaux ;
   la mise en place d'un nouveau système de financement agricole au bénéfice des petites et
   moyennes exploitations agricoles et le renforcement du dispositif d'assurance aux risques et
   calamités agricoles ;
   l'incitation à l'investissement dans les grands projets agricoles dans le sud et les hauts plateaux
   et promotion du partenariat et des Investissements Directs Etrangers, notamment dans les
   cultures stratégiques (céréales oléagineuses, sucre) et dans le développement des capacités
   detrituration et deraffinage ;
   le soutien au développement des capacités de stockage sous froid et les centres de
   conditionnement, notamment dansle sud ;
   le renforcement de l'encadrement professionnel et interprofessionnel desfilières agricoles et
   agroalimentaires;
   le soutien à la professionnalisation des entreprises exportatrices par une mise à niveau et un
   accompagnement rapproché pour développer un marketing soutenu en direction des marchés
   extérieurs.




                                                  33
Autitre del’agriculture de montagne

Un dispositif spécifique pour le développement agricole et rural des zones de montagne sera
développé autour de différentes activités. || s'agit de : (i) l'intensification des cultures existantes et
d'extension del'arboriculture fruitière, (il) la création de petites unités d'élevage (iii) la valorisation
des produits de terroir et des métiers ruraux (iv) et l'encouragement des petites entreprises et des
jeunes porteurs de projets pour les travaux de réalisation des infrastructures de base et
d'aménagement dans les Zones de montagnes.

Aussi, des actions seront prises pour la réhabilitation des forêts, de la nappe alfatière et lutte
contre la désertification par la réhabilitation du barrage vert et l'intensification des actions de
reboisement utile, notamment familial, dans le cadre du programme national de reboisement, au
niveau des zones de montagnes et du barrage vert ainsi que la valorisation durable des
ressources naturelles au profit des populations des zones de montagne et de la steppe.

Autitre del’agriculture saharienne

L'option pourla valorisation des territoires sahariens est fortement affirmée dans l'approche du
gouvernement, qui accorde une attention particulière au développement de ces espaces, à travers
la mise en œuvre d'un ambitieux programme de développement qui s'articule sur:

- l'aménagement des parcours sahariens pour le développement des élevages camelins et
  caprins à travers des actions de réalisation et/ou de réhabilitation des points d'eau et la
   couverture sanitaire des cheptels ;
-_ la valorisation des produits camelins parla création de petites unités de services, d'abattage, de
   mini-laiterieset d'artisanat ;
- la réhabilitation des systèmes oasiens traditionnels et le développement des actions
   structurantes;
-_ la protection etle rajeunissement des palmeraies, la réhabilitation des foggaras, l'acquisition de
   systèmes d'irrigation économes en eau et la rénovation des systèmes de drainage ;
- la réhabilitation des périmètres agricoles et du système oasien moderne en renforçant les
   mécanismes de soutien à l'utilisation des énergies renouvelables.

Autitre de la pêche et des ressources halieutiques

Les activités de la pêche et de l'aquaculture revêtent un caractère stratégique de par leurs
capacités à participerà l'amélioration de la sécurité alimentaire du pays, à travers l'accroissement
et la diversification de l'offre en produits halieutiques de qualité, mais également de par leur
contribution à l'émergence d'une économie nationale productive et diversifiée et à la valorisation
économiqueintégrée de l'espace maritime et du littoral.

Les principaux objectifs ciblés en matière de développement durable des activités de la pêche et
del'aquaculture concernentl'augmentation del'offre nationale en produits halieutiques, à travers,
la promotion et le développement de l'aquaculture à grande échelle et le développement de la
pêche de grand large. En parallèle, uneattention particulière continuera d'être portée à la gestion
durable et responsable des activités de pêche maritime jusqu'ici développées et l'amélioration des
conditions socio-économiquesdes professionnels de la pêche et de l'aquaculture.




                                                    34
Un effort sera également consentià l'accompagnementdel'investissement productif danslesfilières
dela pêcheet del'aquacultureà travers:

- la relance du développementdesindustries nationales dans cesfilières,
-_ l'encouragement à la création de Petites et Moyennes Entreprises (PME-PMI) dans le domaine
   dela transformation et de la valorisation des produits halieutiques,
- la promotion d'une industrie nationale de construction de chantiers navals et d'entretien de la
   flotte.

Aussi, les actions suivantes seront menées pourla redynamisation du secteur de la pêche et des
productions halieutiques (2020-2024), à travers :

La relancede l'élevage halieutique, où l'effort sera axé sur:

-_ l'accompagnementdesprojets d'investissement dans l'aquaculture marine à grande échelle et le
   lancement de l'aquaculture d'eau douce d'entreprise notammentdans les zones continentale et
    saharienne;
-  le renforcementdela valorisation de l'aquaculture dansles plans d'eau, notammentles barrages,
-  la généralisation del'intégration dela pisciculture à l'agriculture ;
-  la promotion de zonesd'activités intégrées d'aquaculture à ériger en pôles d'excellence;
-  la gestion etl'exploitation responsable de la pêche artisanale et côtière, par la mise en place
   effective des outils de gestion durable des productions halieutiques, la promotion et le
   développement de la pêche artisanale, et la relance de l'exploitation durable des pêcheries
   spécifiques (corail rouge, anguille …) ;
-_ renforcerle réseau national de collecte del'information statistique spécifique au secteur.

Parailleurs, il sera procédé à la relance du développementde la production manufacturière locale,
de manière à couvrir les besoins des professionnels de la pêche et de l'aquaculture et de réduire à
terme la facture d'importation desintrants, équipements et matériels de pêche et d'aquaculture.

Dans le cadre du développement de la pêche de grand large, l'effort sera axé sur
l'accompagnement à la création et l'extension d'une flotte dédiée à l'exercice de cette activité, afin
de permettra, l'exploitation de nouvelles zones de pêcheet l'amélioration del'approvisionnement
du marchénational.

Aussi, il sera procédé à l'adaptation et à la mise à niveau des infrastructures et des services
portuaires dédiés pour recevoirla flotte exerçant la pêche au grand large, au moins pourles trois
grandesrégions de la façade maritime (Port de pêche de Sidna Ouchaa à l'Ouest, de Gouraya au
Centre et Annaba à l'Est).

En matière de gestion et de fonctionnement desservices publics dans les ports de pêche,il sera
question de consolider les capacités existantes des entités de gestion de cesinfrastructures ainsi
quela mise en place d'un nouveau mode de gestion de ces entités qui permettra de pérenniserle
développement du secteur de la Pêche et des Productions Halieutiques et du service public des
ports de pêche.

Aussi, il sera procédé à la revitalisation de l'accompagnement des investisseurs pour la mise à
niveau et la création d'unités de conditionnement, de valorisation et de transformation des produits
de la pêche et de l'aquaculture (chaînes de valeur, labellisation des produits de la pêche
artisanale...). || s'agira aussi d'encourageret de faciliter l'exportation des produits transforméset les
produits nobles à haute valeur marchande.


                                                  35
Pour cefaire, || sera nécessaire d'impulser la modernisation du réseau de commercialisation et de
distribution des produits de la pêche et del'aquaculture et l'insertion progressive de ces activités
dans la sphère commerciale formelle, à travers la réalisation des halles à marées au niveau des
ports de pêcheet l'intégration de ces productions dansles circuits commerciaux.

2.11 Pour une véritable industrie du tourisme et de la cinématographie

L'industrie touristique

En matière de promotion du tourisme, le gouvernement a pour objectif de mettre en œuvre un
« Plan destination Algérie » qui s'appuiera principalement surle soutien del'action des agences de
voyage d'une part et la facilitation des procédures de visas au profit des touristes étrangers,
d'autre part.

Aussi, le recours aux formulesincitatives de voyages via des vols charters sera encouragé afin de
renforcerl'attractivité touristique nationale tout en s'appuyant surl'apport de nos représentations
diplomatiques et consulaires à l'étranger.

Par ailleurs, le gouvernement favorisera l'émergence de pôles d'excellence dans le domaine
touristique répondant aux normes et standards internationaux, notamment de nature culturelle,
cultuelle et au niveau des régions du Sud, au regard des potentialités nationales dans ce domaine.

Le gouvernement compte accompagner ses efforts dans le domaine du tourisme par la promotion
desactivités artisanales afin de les ériger en unevéritable industrie etce, à travers :

- la protection des produits del'industrie artisanale nationale et la lutte contre la contrefaçon en
     matière de produits artisanaux, notammentimportés;
-_   le renforcementde l'action de formation à l'endroit des artisans, des petits sous-traitants, des
  coopératives et des groupes professionnels afin d'assurer la mise à niveau nécessaire à une
  production de qualité ;
- la densification des opérations de promotion et de commercialisation des produits issus de
     l'artisanat aussi bien sur le marché national qu'international ;
- la mise en place de nouveaux mécanismes financiers pour soutenir les activités de l'industrie
   artisanale afin d'assurer la pérennité de sa contribution à la mise en œuvre de programmes
   destinés aux artisans (formation, accompagnement, protection, travail communautaire et
   associatif, etc.), en particulier ceux dédiés aux catégories vulnérables et aux régions reculées;
-_ l'organisation et le développementdesactivités artisanales familiales, élément complémentaire
   au renforcement du tissu socioéconomique national.

L'industrie cinématographique

Le gouvernement vise à faire émerger une véritable industrie cinématographique nationale en
privilégiant dans une première phasel'investissement dans la formation des différents métiers du
cinéma, tout en faisant appel aux expériences et compétences internationales pour mettre les
dernières technologies à la disposition des professionnels du cinéma.

Aussi, la création de spécialités dans les profils de techniciens cinématographes et techniciens
d'entretiens de matériels cinématographiques dansles centres de formation sera privilégiée et ce,
afin de renforcerles capacités des métiers de cinéma ainsi que le développement dela production
nationale d'équipements pour le marché du cinéma.

                                                     36
Des mesuresincitatives pour les producteurs étrangers seront mises en place afin de faire de
l'Algérie une destination privilégiée pourles tournagesdefilms et de séries.

En outre, des facilités douanières, d'exonérations fiscales et d'avantages bancaires seront
octroyées au profit des producteurs et des investisseurs en matière de production
cinématographique, de distribution de films et d'exploitation des salles de cinéma. Le partenariat
sera encouragé notamment pour la réalisation de studios d'industrie cinématographique, de
studios de tournage et d'enregistrementet de salles de spectacles.

Le gouvernement s’attèlera à accompagner les professionnels du cinéma et à renforcer la
profession par un dispositif réglementant leur vie professionnelle, de même qu'il encouragera
l'émergence de clubs de cinéma et de mouvements associatifs qui seront considérés comme des
partenaires culturels privilégiés.

Concernantlesinfrastructures, le gouvernement favorisera la réalisation de salles de cinéma et de
multiplexes dans les agglomérations et les centres commerciaux, pour promouvoir la
consommation culturelle.

Enfin, le gouvemement appuieral'investissement dans le domaine cinématographique à travers à
la mise en place de mesuresincitatives et d'un dispositif simplifié pourl'accès au financement
bancaire.

2.12 Développementdesinfrastructures d'appui aux technologies de l'information et de la
communication

Eu égard au rôle incontournable que jouent de plus en plus les TIC dans le développement des
activités économiques, le développement des infrastructures d'appui aux Technologies de
l'information constitue un axe important dansla stratégie gouvernementale visant le renouveau de
l'économie nationale.

L'action du gouvernement portera surl'utilisation optimale des infrastructures existantes et la
réalisation de nouvelles capacités répondant aux normes internationales, supports indispensables
à l'émergence et au développement d'uneréelle économie numérique.

Ainsi, le gouvernement œuvrera à :

- améliorer la qualité de service de la connectivité au profit des citoyens et opérateurs
   économiques;
- généraliser l'accès à l'Internet très haut débit à travers la modernisation ainsi que la
   densification du réseau de télécommunication, la sécurisation des infrastructures des
   télécommunications / TIC et la protection desutilisateurs;
-_ ériger l'Algérie en hub régional en matière de télécommunications / TIC par le développement
   de Datacenters aux normes internationales et la rentabilisation des capacités nationales des
   télécommunications, à savoir la dorsale transsaharienne, les câbles sous-marins et les
   capacités satellitaires ;
-_ développer la société algérienne de l'information par la promotion des contenus numériques
   locaux et la mise en place des mécanismes de gestion et de gouvernance de l'Internet.

Cesefforts seront accompagnés parl'amélioration de la qualité des services offerts et une plus
grande participation des services postaux à l'inclusion sociale et financière à travers le
développement des services et moyens de paiement électronique et l'encouragement de
l'émergence du commerce électronique.
                                               37
2.13 Economie de la connaissanceet transition numérique accélérée

La création d'un nouveau département ministériel dédié à la Start-up et à l'économie de la
connaissance constitue un signal fort quant aux options stratégiques du gouvernement, qui
œuvrera à créer les conditions requises pour une intégration rapide et ordonnée de notre pays
dans l'économie de la connaissance.

A ceteffet, l'action du gouvernementvisera à jeter les bases organisationnelles et fonctionnelles
pour l'émergence d'une économie fondée sur le savoir en s'appuyant sur les entreprises
innovantesetles Start-ups.

Aussiet surla base d'une approche éco systémique visant à développer une culture d'innovation,
il est envisagé de:

-    créer et renforcerles passerelles nécessaires entre les incubateurs, les accélérateurs et les
     pépinières d'entreprises et les universités, et conforter la collaboration entre les différents
     partenaires;
-_ renforcerle partenariat et l'entreprenariat social en tant que levier stratégique par la mise en
   œuvred'actions structurantes en matière de gouvernance numérique.
- soutenir les incubateurs dans l'appropriation des nouvelles technologies et des usages du
   numérique.
- valoriser et tirer profit des expériences de l'élite de la communauté algérienne établie à
   l'étranger dans le domaine de l'accompagnement et du développement des startups;
- encouragerl'instauration deliens de coopération entre les startups algériennes et étrangères;
-_   créer un label incubateur;
-_ mettre en place un guichet unique pourle soutien aux micros entreprises innovantes;
-_ mettre à niveau les cybers parcs et incubateurs existants et créer des incubateurs par secteur
   d'activité (Industrie, Agriculture, Telecom, Hub technologique, …etc.).

Parallèlement, le gouvernement veillera à mettre en place un ensemble de mécanismes
permettantle financementet le soutien de l'économie de la connaissance et les micro-entreprises
et plus particulièrementles Start-up, à travers notamment :

- la promotion et la facilitation de l'accès des Start-up à des sources de financement adaptées,
   notamment parla mise en place d’un fond dédié aux start-up.
-_ l'encouragementet la promotion des investissements en capitaux risque:
-_ l'encadrement juridique du financement participatif, levier important pour le financement des
     Startups:
- la mise en place d'un nouveau cadre légal (Start-up ACT national), dédié au développement
  des Startup, qui définira la start-up, créera les mécanismes de sa labélisation et consacrera un
     régime defacilitation;
- la mise en place de mesures incitatives au profit des start-up (avantages fiscaux et
   parafiscaux) ;
- la promotion etla simplification des procédures d'exportation de services ;
- la facilitation de l'accès des start-up au e-paiement;
- la révision du dispositif légal relatif à la signature électronique en vue de développer les
   services enligne et de dématérialiser les transactions effectuées enligne;
-_ la mise en place d'un statut dédié aux travailleurslibres (freelancer) pourfaciliterle recours des
   Start-up à une ressource humaine spécialisée ;

                                                  38
 la création d'une nouvelle catégorie de statut juridique, plus flexible et mieux adaptée aux
 Start-up ;
 la création d'un fonds dédié à l'appui au transfert de technologie ;
 l'initiation de mesuresincitatives en direction des capitaux d'investissement, de la diaspora et
 l'actualisation des conventions pour la non double imposition ;
 la révision et l'actualisation des avantages fiscaux pour les projets industriels bâtis sur des
 stratégies d'innovation et ou des centres R&D:
 la création du Small business Act algérien pour permettre aux micro-entreprises l'accès aux
 marchéssousla forme d’une bourse de sous-traitance des PME innovantes;
 la promotion du rôle des incubateurs au niveau de l'écosystème intégré, à savoir : universités,
 incubateurs spécialisés, médiateurs facilitateurs ou accélérateurs, pépinières de startups,
 entreprises et opérateurs publics, banques, centres de recherche et espaces de coaching et
 travail collaboratif ;
 le soutien à la création de PME micro-entreprises et Start-up au niveau universitaire via les
 incubateurs universitaires et la conversion en produits des projets de recherche à fort potentiel
 industriel.

. Une approche économique pourlutter contre le chômage et promouvoir l'emploi

La promotion de l'emploi et la lutte contre le chômage continueront à être les objectifs
stratégiques de la politique nationale de développement durant la période 2020-2024, à travers
les efforts visant la construction d'une économie émergente diversifiée, créatrice d'emplois
décents et de richesses, reposant sur une stratégie de performance et de développement
durable,inclusif et équitable.

Le gouvernementest conscient quele facteur fondamental qui a favorisé et exacerbéle chômage
dans notre pays demeure le déséquilibre des qualifications entreles produits du système éducatif
et de formation et les besoins du monde économique et du marché de l'emploi, induisant un
chômage structurel des jeunes particulièrement et le développement de l'informel dans
l'économie.

L'autre élément réside dansla faiblesse de la croissance économique du pays, qui peine à créer
suffisamment de postes d'emplois permettant d'absorber la population jeune en forte
augmentation.

Partant de ce constat, le gouvernement est résolu à tirer pleinement profit du potentiel humain
que représente notre jeunesse, à travers l'adoption d'une nouvelle démarche purement
économique, basée sur la croissance comme moteur principal de création d'emplois durables, qui
marque une rupture avec l'approche jusque-là adoptée qui privilégiait un traitement purement
social du chômage parla création artificielle de postes d'emploi précaires.

Le gouvernement ambitionne ainsi de réduire, à très court terme, le taux de chômage sous la
barre de 10% et d'accroitre sensiblementla part de l'emploi salarié permanent durantla période
2020-2024.

3.1 Adéquation des programmesde formation avec les besoins du marché du travail

Le gouvernement œuvrera à améliorer les performances qualitatives du système
éducatif national, de l’enseignement technique et de la formation professionnelle de manière à
l'adapter à l'environnement économique du pays et de mettre en adéquation le capital humain
avec les besoins du marché du travail, dans l'objectif de renforcerl'efficacité et les rendements
de notre économie et favoriserl'insertion de notre société dansl'économie moderne.
                                               39
La période 2020-2024 connaitra la modemisation du secteur de la formation et de l'enseignement
professionnels afin de former une ressource humaine de qualité, susceptible de contribuer au
développement socioéconomique,à travers:

 l'adaptation des formations aux besoins des secteurs économiquesprioritaires;
 la mise en place de filières d'excellence dans les métiers du bâtiment, de l'agriculture, de
 l'industrie et des TIC ;
 le renforcement des mécanismes de concertation intersectorielle afin d'améliorerl'employabilité
 des demandeurs d'emploi, à travers une politique de formation et d'apprentissage à la carte ;
 la promotion et le développement de la formation professionnelle continue afin d'améliorer
 l'employabilité des travailleurs et la compétitivité des entreprises.

3.2 La promotion de l'emploi

La politique de l'emploi qui sera désormais mise en œuvre par le gouvernement ne se contentera
pas de la gestion de la phase de primo-insertion pour les diplômés, mais sera étendue à la
gestion destransitions dans le marché du travail, notammentl'aspect lié à la mobilité.

Le gouvernement veillera à un meilleur fonctionnement du marché de travail parl'ajustement de
l'écart entrel'offre et la demande, à travers la mise en place de nouveaux mécanismesd'aide à
l'insertion professionnelle. Ces mécanismes seront désormais orientés exclusivement vers le
secteur économique, de manière à apporter plus de cohérence.

Cette approche, fondée surle traitement économique du chômage devra répondre aux attentes
desjeunes, en termes de postes d'emploi permanents et de couverture sociale et aux besoins
des employeurs.

Le gouvernement œuvrera égalementà favoriserla création d'emplois à travers la consolidation
des mesures incitatives, notamment les exonérations fiscales et la diminution des charges
patronales.

Il sera également procédé à l'amélioration de l'efficience des mécanismes de prospection et
d'intermédiation publics et privés sur le marché de l'emploi afin de garantir plus d'interopérabilité
entre ces deux voies d'intermédiation.

3.3 Soutien à la création d'activités

Il s'agira principalement de stimuler l'investissement et la création d'activités génératrices
d'emplois dans les secteurs créateurs d'emploi, tels quel’agriculture, l'industrie, le numérique,le
tourisme etl'artisanat et de favoriser le développementde la petite et moyenneentreprise et les
Start-up.

Les dispositifs de création d'activités seront renforcés afin de permettre le soutien des
promoteurs en difficulté et le renforcement de l'accompagnement des porteurs de projets, à
travers un suivi personnalisé.

Un nouveau dispositif "congé création d'entreprise", sera créé au profit des salariés, détenteurs
de compétences et de savoirs faire dans leur domaine d'activité pour stimuler et encourager
l’entrepreneuriat au sein decette frange.

                                                40
Le gouvernementinscrit également comme axe prioritaire de son plan d'action le renforcement
de l'intégration économique des femmes, des personnes à besoins spécifiques et des
demandeurs d'emploi résidant au niveau des localités dépourvues de bassin d'emploi, à travers
la mise en place de nouveaux mécanismesincitatifs.

Enfin, l'établissement d'une cartographie et d'un système d'information précis et détaillé du
marché et del'économie nationale, basée surles informations recueillies auprès des différentes
institutions concernées, permettra une meilleure analyse et anticipation en matière de
financement de micros activités.




                                             41
                                      CHAPITRE TROISIEME
             POUR UN DEVELOPPEMENT HUMAIN ET UNE POLITIQUE SOCIALE


1. Développement humain

1.1 Éducation
 L'action du gouvernement sera orientée essentiellement vers la rénovation del'institution scolaire
 qui compte un parc infrastructurel de plus de 27.634 établissements accueillant 9.597.267 élèves,
 encadrés par 479.081 enseignants et 250.533 administratifs.

 Il s'agira de renforcer les actions en matière de démocratisation et d'approfondissement de
 l'enseignement en vue d'assurer, non seulement l'égal accès de tous à l'éducation, mais
 égalementla réussite du plus grand nombre, dans un système éducatif fondé sur l'équité, la
 qualité et les valeurs éducatives de moralité et d'universalité, faisant de « l'école le meilleur
 moyen d'ascension sociale ».

 La concrétisation de ces objectifs se fera à travers un ensemble d'actions destinées à adapterle
 processus de réforme aux nouvelles exigences etintroduire les améliorations quis'imposent.

 Cesactions s’articulent autour des axes suivants:

En matière d'obligation de scolarité

- assurerla scolarité obligatoire des élèves non-scolarisés et en abandon scolaire et généraliser
   progressivementl'éducation préparatoire ;
-_ prendre en charge les élèves à besoins spécifiques, assurerl'accompagnement pédagogique
   des élèves en difficulté scolaire ou hospitalisés et mettre en place un dispositif de formation des
   enseignants, chargés des classes multi-niveaux ;
-_ réduireles disparitésintra et inter-wilayas en matière de paramètres de scolarisation.

En matière de refonte pédagogique

- mettre en cohérence les programmes pédagogiques afin d'améliorer leur pertinence et la
  qualité des apprentissages, réviser les programmes scolaires, notamment dans le cycle
  primaire et actualiser les méthodes d'enseignement/apprentissage de manière régulière,afin de
  donnerà l'élève la possibilité d'acquérir des compétences et des savoir-faire transférables dans
   des situations de vie ;
- renforcer les activités d'éveil dans le cycle primaire et les activités périscolaires, culturelles et
  sportives dans les différents cycles d'enseignement, en assurant les ressources didactiques
   nécessaires;
- élaborer de nouveaux manuels scolaires allégés à même d’atténuerla lourdeurdu cartable des
   apprenants ;
-_ promouvoir lesfilières mathématiques, techniques mathématiques et scientifiques ainsi que
   l'enseignement de l'informatique, en tenant compte des exigences du développement
   économiqueet technologique ;
- renforcer l'enseignement et la formation à distance en procédant au redéploiement des
   missions del'Office Nationale de l'Enseignementet de la Formation à Distance ;

                                                  42
- élargir l'enseignement de Tamazight dans sesdifférentes variantes;
-_ réduire la déperdition scolaire en renforçant le dispositif de la guidance scolaire en vue d'une
   meilleure prise en charge pédagogique, psychologique et sociale des élèves;
- approfondir la maitrise des apprentissages del'enseignement fondamental ;
-_intensifier l'intégration des TIC et des TICE et mettre en place un nouveau dispositif
   d'évaluation pédagogique des apprentissages.

En matière d'amélioration de la gouvernance du système éducatif

- soutenir les actions de numérisation et de développement du système d'information, afin
    d'assurerefficacité, traçabilité et transparence;
-_ renforcer les capacités d'accueil par l'inscription des projets d’infrastructures pédagogiques et
   de soutien, afin d'améliorer les paramètres de scolarisation (temps scolaire, taux d'occupation
    deslocaux, taux d'encadrement...) et réduire la double vacation dansle cycle primaire ;
- soutenir la stratégie nationale de prévention et delutte contrela violence en milieu scolaire ;
-_ développerle système de collecte et d'analyse des résultats des élèves aux examens scolaires
    et aux évaluationsnationales etinternationales;
-_ prendre en charge les préoccupations de la communauté éducative et optimiser les ressources
    humaines, financières et matérielles.

Dansle domaine de la professionnalisation des personnels par la formation

-_ élaborer desréférentiels de compétences professionnelles et des plans de formation au profit
   de chaque corps de métier et la promotion de la formation à distance des personnels de
    l'éducation ;
-_ densifier le réseau des établissements de formation des personnels (Instituts nationaux de
   formation des fonctionnaires du secteur de l'éducation nationale INFFSEN) et renforcer leurs
   missions statutaires.

En matière de soutien à la scolarité

-   amélioreret renforcerle transport scolaire des élèves, les structures d'internat, de demi-pension
    et de cantinesscolaires ;
-_ soutenir les actions de solidarité scolaire au profit des élèves nécessiteux (prime de scolarité,
   trousseaux scolaires et gratuité du manuel) ;
-_ développerl'éducation sanitaire et le bon fonctionnement des Unités de Dépistage et de Suivi
    (UDS).
En matière de dialoguesocial

Il s'agira de renforcerle dialogueet la concertation avec les partenaires sociaux etl’organisation
de rencontres régulières, en vue de favoriser un climat de mobilisation, de résolution des
problèmes et d'amélioration des conditions de travail.

1.2. Enseignement supérieur

Notre pays sera amené à évoluer dans un contexte marqué parl'accélération de la mondialisation
et l'émergence de nouvelles puissancesintellectuelles ; et dans un tel contexte, les universités, les
grandesécolesetles centres de recherche seront fortement soutenus pour assurerleur adaptation
afin de devenir un cadre d'enseignement, d'épanouissement et d'innovation et constituer de
véritablesleviers de développement del'économie de la connaissance.

                                                   43
Les transformations nécessaires seront engagées pour assurer un enseignement de qualité, la
formation d'élites et de pôles de recherche de référence, à même de répondre aux attentes des
entreprises, en leuroffrant la ressource humaine devantleur permettre de se positionner dans un
marché mondialisé.

Pour atteindre ces objectifs, le gouvernement s'attèlera à :
   favoriser le développement des pôles d'excellence, en partenariat avec l'entreprise, dans des
   spécialités en adéquation avec le développementuniversel des technologies et des métiers et
   avecles besoins de l'économie nationale ;
   améliorer les performances et la gouvernance de l’université et son ouverture sur
   l'environnementnational et international, en fixant les missions dans des cahiers des charges
   précis et adaptés aux besoins nationaux ;
   réviser la carte de formation des domaines etfilières pour permettre l'adéquation de cursus
   avec les besoins socio-économiques et les exigences en matière d'encadrement et
   d'infrastructures;
   mobiliser la communauté universitaire autour de la nécessité de consolider la philosophie de la
   charte d'éthique et de déontologie, afin de raffermir la confiance entre les différents acteurs de
   l'université, de réhabiliter graduellement l'image del’université et de renforcerle respect pourle
   savoir ;
   valoriser les fonctions et renforcer les prérogatives de la pédagogie et de la recherche au
   niveau de l'organisation des établissements d'enseignement supérieur et en améliorant leur
   statut, notammentà travers la mise en place de Comités Pédagogiques Nationaux, parfilières ;
   accroitre le taux d'utilisation des bourses universitaires à l'étranger, multiplier les échanges
   universitaires et les jumelages entre universités, diversifier les partenaires et encourager le
   tourisme scientifique, notamment dansle palier du doctorat ;
   développerla recherche scientifique et technologique et valoriser le produit de la recherche à
   travers le concept de «la recherche à la demande » et les contrats, par les universités et
   centres de recherche avec les entreprises économiques, afin de commercialiser les produits,
   sous forme de PME. À cetitre, la création d'entreprises par les chercheurs et les étudiants
   diplômés (start-ups) sera encouragée ;
   orienterl'université vers les métiers du futur, l'enseignement de l'intelligenceartificielle, l'internet
   des objets, la médecine 2.0, les grandes mutations économiques et géopolitiques, la robotique,
   le développement humain durable et la transition énergétique nécessaire pour sortir du « tout
   hydrocarbures »;
   privilégier graduellement la formation technologique en intégrant le cursus de formation
   d'ingénieurs au niveau du parcours LMD pourcertaines spécialités des sciences techniques et
   technologiqueset réhabiliter et valoriserla mission de formation continue;
   accompagner l'université dans son processus de développement qui mettra, au centre des
   préoccupations, la qualité de l'acte pédagogique, lequel s'appuiera surla rigueuret l'objectivité
   scientifiques où tout mérite ne saura être attribué sans efforts ni résultats évalués
   scientifiquement, notamment à travers la séparation de l'administratif de la gestion de l'acte
   pédagogique qui demeure du ressort du conseil scientifique;
   mettre en place le Conseil National de la Recherche Scientifique et des Technologieset en faire
   un vecteur de développement socio-économique ;
   engager uneréorganisation du système des œuvres universitaires en vue d'assurerà l'étudiant
   un cadre de vie décent et de qualité en termes d'hébergement, de restauration et de transport ;
   promouvoir le sport universitaire à travers la réalisation de structures dans les campus
   universitaires et l'organisation de compétitions interuniversitaires régionales et nationales;
   promouvoir la culture scientifique et universitaire au sein du grand public, à travers notamment
   la densification del’organisation de conférencesnationales.
                                                    44
1.3. Formation professionnelle

Dans ce cadre, les actions du plan d'action du gouvernement s’articulent autour de
l'amélioration de la qualité de la formation et le renforcement de la formation et de
l’enseignement technique, scientifique et technologique, à travers :

- la promotion des filières de formation techniques, scientifiques et technologiques et la
   réorganisation du cursus d'enseignementprofessionnel et du systèmed'orientation, du certificat
   de formation professionnelle spécialisée (CFPS) au diplôme de brevet de technicien supérieur
   (BTS);
-_ l'élargissement du réseau d'infrastructures de formation et la création defilières d'excellence
   dans les branches prioritaires de l'économie nationale, à travers le développement du
   partenariat et la redynamisation des établissements d'ingénierie pédagogique:
- le développement de la formation par apprentissage et de la formation à distance et le
   renforcement del'offre de formation destinée aux personnes aux besoins spécifiques et en
     milieu carcéral;
-_   l'introduction des languesfonctionnelles, notammentl'anglais, au niveau des établissements de
   formation et d'enseignement professionnels ;
-_ la mise en place d'un programme de coopération et d'échangeset de projets de jumelage entre
   les établissements et avec les établissements de pays leaders dans le domaine dela formation
   et del’enseignementprofessionnels ;
- l'élaboration et la mise en œuvre d'un programme annuel et pluriannuel de formation, de
   perfectionnementet de recyclage des personnels du secteur;
- le développement des capacités de formation des établissements privés et leur
     accompagnement;
-_ la valorisation du corps des enseignants et des personnels de la formation et del'enseignement
   professionnels et l'amélioration de leurs conditions detravail.

La mise en œuvre du baccalauréat professionnel, par la révision du système
actuel d'orientation vers le cursus d'enseignement professionnel, la mise en place d'uneingénierie
pédagogique propre au baccalauréat professionnel et la création d'un office des examens et
concours dela formation et del'enseignement professionnels.

La modernisation et la numérisation des activités, à travers:

- l'actualisation des instruments de gestion pédagogique, administrative, comptable et
  financière etla poursuite du programme de numérisation des actes de gestion ;
- la mise en place d'un nouveau système de suivi pédagogique complémentaire et d'un système
  d'information statistique interne et externe.

L'amélioration del’environnement et des conditions de travail, par:

- l'aménagement et l'équipement des établissements de formation et d'enseignement
     professionnels ;
- la prise en charge du transport des stagiaires au niveau des zones rurales et enclavées,
  notamment pourles femmes et les personnes à besoins spécifiques;
-    l'introduction, au niveau des établissements de formation et d'enseignement professionnels, des
     activités sportives, culturelles et de loisirs.




                                                 45
1.4. Santé et accessibilité aux soins :

Les principaux défis auxquels est confronté notre système de santé résultent de la transition
sanitaire et démographique que traverse notre pays, qui demeure attaché au principe de la
protection et de la promotion de la santé des citoyens et à l'objectif d'atteindre la couverture
sanitaire universelle et de placer l'Algérie, à l'horizon 2030, dans la moyenne intemationale
{benchmark) pourl'ensemble desindicateurs de référence en matière de santé publique.

Le plan d'action du gouvernement, dans le domaine de la santé, concernera principalement les
mesuresrelatives au renforcement et à l'organisation del'offre de soins, dont les grandeslignes
portent sur un système de santé planifié pour le rapprochement de la santé du citoyen, la
hiérarchisation des soins, le renforcement de la prévention et des soins de proximité, la prise en
charge de la transition épidémiologique et des disparités géographiques et ce, dans l'objectif
d'assurer des services de qualité, dans le respect dela dignité des malades.

Ainsi, les axes prioritaires de ce plan d'action du gouvernement se déclinent comme suit:

L'humanisation del’activité de santé, par:

-   l'amélioration del'accueil et de l'humanisation dans les établissements de santé, notamment au
    niveau des services des urgences médico-chirurgicales;
-   la sensibilisation, la mobilisation et la motivation des professionnels de la santé.

La couverturesanitaire de la population

a- En matière de normalisation, de gouvemance et d'organisation de l'offre de soins :

-_ la mise en place de la carte sanitaire et du schéma d'organisation sanitaire ;
-_ le regroupement d'entités au niveau local, afin de répondre aux besoins de santé de manière
   intégrée, hiérarchisée et permanente au niveau d'uneaire géo-sanitaire précise, en mutualisant
    les ressources humaines et matérielles;
-   la normalisation, à tous les niveaux, afin de disposer de structures performantes avec des
   plateaux techniques standardisés et une répartition équitable en moyens humains et matériels,
   garantissant un accès équitable aux services de santé ;
-_ le renforcement des capacités des structures de proximité et desinfrastructures hospitalières et
   le développementde la santé de proximité parl'externalisation des consultations spécialisées et
    la concrétisation de la notion de médecin référent;
-_ le renforcement de l'organisation et du fonctionnement des structures d'urgences, à travers la
   normalisation, la mise en place des réseaux de prise en charge hiérarchisée, le renforcement
   des points d'urgences de proximité et la réorganisation de l'EAMU (Etablissement d'Aide
   Médicale d'Urgence) et des SAMU ;
-_ le renforcement des programmes de soins relatifs aux problèmes de santé publique tels que :
   infarctus du myocarde, accidents vasculaires cérébraux, pied diabétique, hémodialyse, greffe
    d'organes, hépatites, sclérose en plaque, … ;
-_ l'amélioration du partenariat entre les différentes structures et établissements de santé et le
   développement del'intersectorialité ;
-_ la normalisation del'activité au niveau du secteurprivé en tant que secteur complémentaire au
    secteur public et l'évaluation de son activité ;
- le développement du transport aérien dédié aux évacuations pourle Sud etles Hauts Plateaux ;


                                                       46
- le renforcement de l’approvisionnement adéquat des établissements de santé en médicaments
  et en équipements, en garantissant la qualité, l'efficacité et l'innocuité des produits
  pharmaceutiques, des dispositifs médicaux et des vaccins ;
- le développement d'un système d’information sanitaire performant, en intégrantles technologies
  del'information et de la communication ainsi que le développement des plateformes internes et
   externespourles établissements de santé et de télémédecine;
- la répartition rationnelle despraticiens spécialistes au niveau national.

b- En matière de médicaments : le gouvernement œuvrera à réunir toutes les conditions
permettant de garantir la disponibilité continue des produits pharmaceutiques, notamment les
médicaments essentiels. || veillera à mettre en place les outils nécessaires et un dispositif
réglementaire garant de la qualité, de l'efficacité et de la sécurité, en promulguant les textes
réglementairesrelatifs au fonctionnement del'Agence Nationale des Produits Pharmaceutiques et
aux statuts des établissements pharmaceutiques.

c- En matière de formation:

- le renforcement de la formation des professionnels de la santé pour répondre aux besoins en
  termes depluridisciplinarité et le développementdel'évaluation de la qualité des pratiques et de
   la sécurité dessoins ;
- la mise en place d'un programme spécial de formation pour certains professionnels, afin de
  pallier au déficit actuel qui concerne les AMAR, les sages-femmes, les accoucheusesrurales et
  les manipulateurs radio et le développement d'autres filières de formation pour s'adapter à
  l'évolution technologique;
- la mise en place d'un système d'évaluation et d'audit en santé ainsi que des projets de service,
  projets d'établissement, des contrats d'objectifs et de performance, ceci dans un souci de
  planification et de maitrise des dépenses.

d- Dans le domaine de la prévention et de la lutte contre les maladies transmissibles : en
consolidant le programme de prévention et de lutte contre les maladies contrôlables par la
vaccination, pour lequel il s'agira notamment de veiller à (i) maintenir l'Algérie « polio free zone »
et sans tétanos maternel et néonatal, (ii) éliminer la diphtérie, la rougeole et la rubéole et la
consolidation del'inversion de la tendance évolutive de la tuberculose contagieuse.

e- Dans le domaine de la prise en charge des maladies non transmissibles et des risques
sanitairesliés à l'environnementet au climat :

- la création de réseaux de soins avec hiérarchisation de la prise en charge (urgences cardio-vx,
   cancer, réanimation, AVC, femmegestante, ….) :
- la promotion du dépistage des cancers, la consolidation du réseau national des registres de
   canceretla réduction del'impact environnemental surla santé ;
- la redynamisation du plan national de promotion de la santé mentale et le développement de la
  greffe d'organes.

f- Dansle domaine dela couverture sanitaire dans le Sud et les Hauts Plateaux :
- le développementet le renforcement des programmes spécifiques aux wilayas du Sud et des
  Hauts Plateaux à savoir : (i) programme delutte contre les MTH, (ii) programme de lutte contre
  la leishmaniose cutanée,(ii) l'envenimation scorpionique, (iv) le trachome,(v) le paludisme et
  autres maladies à transmission vectorielle (vi) les foyers résiduels de bilharziose;

                                                 47
    le renforcement de la surveillance épidémiologique au niveau des wilayas frontalières, compte
    tenu des menacessanitaires émergentes et ré-émergentes à potentiel épidémique;
    la création de l'observatoire des maladies tropicales à Tamanrasset;
    le développement des capacités d'intervention des équipes médicales fixes et mobiles dans le
    domaine de la détection précoce et de la riposte rapide aux phénomènes épidémiques;
    le renforcement du dépistage des maladies radio-induites dans la population générale des
    wilayas d'Adraret de Tamanrasset ;
    le renforcement du dispositif de jumelage et de télémédecine entre les établissements de santé
    du Sud et des Hauts Plateaux avec ceux du Nord, pour assurer des soins spécialisés de
    proximité   ;
    l'amélioration des conditions et des mesures incitatives pour les professionnels de santé au
    niveau des régions du Sud et des Hauts Plateaux.

g- Dans le domaine de la santé matemnelle et infantile :

-   la réduction accélérée des décès maternels et la consolidation del'audit des décès maternels ;
-_ la mise en œuvre du plan national de réduction accélérée de la mortalité néonatale.

h- En matière de financement: formaliser le système de contractualisation entre les
établissements de santé et les organismes de sécurité sociale.

En matière de population : la politique sera renforcée dans son approche multisectorielle et
pluridisciplinaire, tant au niveau national qu'au niveau local, intégrantà la fois les secteurs del'Etat
et la société civile. Elle œuvrera à consolider et à améliorer l'intégration de la variable
démographique dans les stratégies de développement économique et social, en vue d'assurer
l'équilibre entre les ressources humaines, les ressources économiqueset l’environnement.

En matière d'industries pharmaceutiques : il s'agira de développer les industries du
médicament et autres produits à usage médical pouratteindre 70% de la production locale sur les
génériques et 30% en produits de spécialité et ériger l'industrie pharmaceutique nationale en
secteur créateur de richesses.

A ce titre, le plan du gouvernement a retenu les actions suivantes:

-_ l'actualisation du cadre réglementaire relatif à l'investissement dans la production et la R&D
   pharmaceutiqueset à la sous-traitancelocale des intrants pharmaceutiquesainsi qu'aux études
   cliniques, afin de permettre le développement de ce segment;
- la réalisation d'une cartographie des sites de production et l'orientation des projets de
   production pharmaceutique vers les gammes de produits essentiels et ceux à forte valeur
   ajoutée encore importés, afin de maitriserla facture d'importation et de préserver les équilibres
    financiers des caisses de sécurité sociale;
- la révision du cadre réglementaire relatif à l'importation des produits pharmaceutiques pour
  protégerla production locale et pérenniserl’approvisionnement du marché ;
- le renforcement du contrôle des établissements de production pour s'assurer de la conformité
  aux exigences des bonnes pratiques de fabrication et la définition du cadre réglementaire en
  matière d'exigence d'essai de bioéquivalence pour les médicaments génériques et les
  conditions d'autorisation d'un centre de bioéquivalence.




                                                  48
1.5. Culture :

Le plan d'action du gouvernement reposera sur une démarche visant à soutenir et à accompagner
la création artistiqueet l'entreprenariat culturel, à travers la mise à disposition au profit des artistes
et des créateurs, d'espaces dédiés dans les friches industrielles et dans les espaces libres.
Il œuvrera à valoriser le métier d'artiste et de tous les acteurs de la culture et à promouvoir leur
rôle etleurs statuts sociaux.

Dans ce cadre, il s'engage à rentabiliser et à optimiser toutes les potentialités du réseau
d'infrastructuresliées à la diffusion culturelle : salles de spectacles, théâtres, salles de cinémas,
musées, etc.

Le gouvernementveillera aussi à soutenir et à consoliderles initiatives lancées par les jeunes
artistes, à travers desdispositifs d'accompagnement et de promotion deleurcréation, à assurerla
défense des droits d'auteurs et des droits voisins et à lutter contre le piratage des œuvres
artistiques.

Le plan d'action du gouvernement accorde également une plus grande place à la culture en milieu
scolaire, mettant l'école au centre du développement des activités artistiques et culturelles en
l'érigeant en « couveuse » des arts et dela créativité. A cetitre,il prévoit notamment:

-_ l'organisation d'ateliers de théâtre et deréflexion participative pour enfants et d'ateliers du livre,
   à traversla participation des associations pourla formation des jeunesà l'animation culturelle et
   artistique ;
- l'organisation régulière de spectacles cinématographiques, de représentations théâtrales et
  d'expositions artistiques dans les écoles, en consacrant la pratique « d’un film dans chaque
  école » et « une pièce théâtrale par école », ainsi que la création de clubs de cinéma au niveau
   deslycées et des universités;
- la promotion dulivre et dela lecture et la création de bibliothèques électroniques.

Le gouvernement s'attellera aussi à créer un environnement favorable à l'émergence et à
l'éclosion des dons et des talents artistiques, notamment par la promotion de cursus scolaires et
universitaires artistiques et la création d'un baccalauréatartistique.

Enfin, la dimension relative à la préservation, la protection et la promotion du patrimoine national
culturel, matériel et immatériel, bénéficiera de toute l'attention requise.

1.6. Promotion des activités physiques et sportives et du sport d'élite :

Les actions du gouvernements'inscrivent dans la cadre dela refonte del'organisation desactivités
physiques et sportives. Elles visent à promouvoir le sport en milieu scolaire et universitaire, le sport
communalainsi que les handisports et le sport féminin.

Elles bénéficieront d'un accompagnement soutenu du gouvernement par la réalisation
d'équipements sportifs d'excellence et de proximité sur l'ensemble du territoire national et
l'amélioration de leurs dispositifs de gestion dans le sens d’une plus grande flexibilité et d'une
implication effective des associations de quartiers.




                                                   49
La place et la mission de l'éducation physique et sportive seront renforcées par la révision des
rythmes d'études, le renforcement du parc infrastructurel et la mobilisation de ressources
pédagogiques, notamment au niveau du premier palier qui constitue un véritable vivier pour
l'émergence des jeunestalents sportifs, garants du renouvèlement del'élite sportive nationale.

Elles visent la promotion d'une véritable politique de détection et de formation des jeunes talents
sportifs, à travers les structures spécialisées créées à cet effet (lycées sportifs, centres de
préparation del'élite et écoles nationales), ainsi que le développement du sport professionnel qui
s'inscrit, également, dans la même dynamique des objectifs stratégiques du plan d'action du
gouvemement.

Dans le même cadre, la promotion et la valorisation du sport d'élite, source de rayonnement de
notre pays dans le monde, sera hissée au rang de priorité du gouvernement. La nomination d'un
Secrétaire d'Etat chargé du Sport d'élite s'inscrit dans cet objectif.

Partant d'un diagnostic établi en relation avec le mouvement sportif national, les actions dédiées à
l'élite sportive prennent en charge les différents objectifs à moyen et long termes, notammentles
objectifs prioritaires : les Jeux Olympiques et Paralympiques de Tokyo 2020 ainsi que les Jeux
Méditerranéens Oran 2021.

Le plan d'action vise en outre la mise en cohérence des moyens mis en place par les pouvoirs
publics sur le plan financier, la formation de l'encadrement de hautniveau, l'application du statut
de l'élite et de haut niveau ainsi que la promotion de la médecine du sport et la création du
laboratoire anti dopage. De même qu'il sera procédé au renforcement du tissu d'infrastructures
sportives de haut standing pourla préparation des athlètes d'élite.

Par ailleurs, la moralisation et la bonne gouvernance dans le sport feront l'objet d'une attention
particulière du gouvernement. Des actions de prévention et de sensibilisation seront engagées en
relation avec le mouvement sportif associatif et en conformité avec les dispositions légales et
réglementaires. À ce titre, des programmes de prévention et de lutte contre la violence dans les
milieux sportifs seront développés.

Concernantl'amélioration de la couverture en matière d'infrastructures, il est retenu :

-   la livraison à la fin de cette année, des différents programmes d'investissement, notammentles
    grands projets de stades devant accueillir les manifestations internationales à court terme (Jeux
    méditerranéens 2021 à Oran et championnat d'Afrique de football des joueurs locaux en 2022,
    au niveau de 04 villes) ;
- la mise à la disposition de l'Elite sportive et de haut niveau des infrastructures
   multidisciplinaires, en adoptant une gestion souple etefficace;
-_ l'assistance des clubs professionnels par la concession de stade et l'octroi de terrains pourla
   réalisation deleurs stadeset infrastructures sportives, pourla rentabilisation des clubs ainsi que
    la diversification desfinancements des sports ;
- l'accompagnement des clubs professionnels pour disposer de leurs propres centres de
    formation ;
- l'élaboration et la mise en œuvre d'un plan de préservation et d'optimisation du parc
  infrastructurel existant(plus de 6.000 structures de sport), à travers sa réhabilitation et sa mise
    à niveau;
- la mise en place des structures économiques de gestion et d'exploitation des grands ensembles
  sportifs.
                                                  50
2. Politique sociale :

2.1. Accroissementet consolidation du pouvoir d'achat du citoyen:

Le gouvernement s'engage à revaloriser le salaire national minimum garant (SNMG), afin
d'assurer au citoyen un revenu décent.

A cetitre, le gouvernement engagera, à travers les mécanismes de concertation avecles différents
acteurs et partenaires sociaux et, notamment, la tenue de rencontres tripartites, la détermination
d'un nouveau salaire national minimum garanti (SNMG) permettant l'amélioration du pouvoir
d'achat descitoyens et élaborera les textes nécessaires.

Dans ce même cadre, des mesures de défiscalisation serontprises à l'endroit des bas revenus.

Pour cefaire, le gouvernement engagera une évaluation des politiques salariales parla réalisation
d'enquêtes:

   - sur les niveaux des salaires dans le secteur économique pour disposer d'indicateurs
     pertinents surles pratiques et tendancessalariales dans le secteur économique permettant le
    suivi desrevenus;
   - sur le système des rémunérations dans la fonction publique pour l'adapter et répondre aux
     enjeux d’attractivité dela fonction publique et y introduire les réformes nécessaires.

2.2. Prise en charge des populations vulnérables

L'État œuvre à garantir la promotion et l'amélioration de la prise en charge des catégories
vulnérables, en développant des mécanismes transparents assurant le ciblage optimal des
véritables bénéficiaires, dans un cadre légal et en impliquant le mouvement associatif, tout en
renforçant son soutien à la politique tendant à encouragerl'insertion dansla vie active.

À cet effet, les actions du plan du gouvernementvisent:

La protection et la promotion des personnes à besoins spécifiqueset leur insertion dans la
vie active, par l'adoption d'une approche intersectorielle et en collaboration avec le conseil
national des personnes à besoins spécifiques et ce,à l'effet :

   - d'actualiser le système juridique actuel par sa mise en conformité avec les préoccupations
     actuelles des personnes à besoins spécifiques et avec les principes fondamentaux de la
     convention internationale relative aux droits des personnes à besoins spécifiques;
   - de faciliter les conditions de l'insertion professionnelle et sociale des personnes à besoins
     spécifiques, notamment à traversl'application de la règle de réservation de 3% des postes de
     travail au profit des personnes à besoins spécifiques, l'adaptation des espaces et des postes
     de travail et la création des établissements d'aide par le travail et les ateliers protégés, ainsi
     que la mise en place de mécanismes de commercialisation des produits réalisés par ces
    établissements ;
   - d'adopter des mécanismes de contrôle et de suivi à l'effet d'éviter le double bénéfice des
    aides sociales à travers des opérations d'assainissement;
   - de paracheverle dispositif devant faire bénéficier les personnes handicapées à 100% de la
    réduction dela location et del'achat des logements sociaux ;


                                                  51
   - d'assurer l'éducation et l'enseignement spécialisés en faveur des enfants et adolescents à
     besoins spécifiques et encouragerleurintégration dans le milieu scolaire ordinaire, à travers
    l'ouverture de classes spéciales etla formation des encadreurs ;
   - de renforcer les canaux de dialogue avec les associations et les différents organismes et
     conseils consultatifs en créant descellules d'écoute spécialisées.

La protection et le bien-être des personnes âgées dont l'intégration en milieu familial constitue
unepriorité visant à assurer la cohésion sociale. Pour cela le gouvernement s'engage à :

   - encourager la médiation familiale et fournir une assistance pour la prise en charge des
    ascendants par leurs descendants:
   - consolider les canaux de communication intergénérationnelle en organisant des visites
     régulières des enfants et des jeunesauxfoyers des personnes âgées ;
   - renforcer le dispositif de prise en charge des personnes âgées malades mentales résidant
     dansles foyers pour personnes âgées.

La protection et la promotion del’enfance et del'adolescence à travers les programmes de
solidarité en vue de préserverl'intérêt suprême de l'enfant et garantir son épanouissement. Dans
ce cadre, le gouvernement œuvrera à :

   - consolider les mesures visant à développer les capacités de l'enfant et à promouvoir sa
     participation et assurer sa protection des mauvais traitements et des dangers physiques et
    moraux et de toute forme d'exploitation, notammentle travail des enfants et leur utilisation
    dans la mendicité ;
   - renforcer les mécanismes de placement en milieu familial et ce, à travers la promotion du
     système du recueil légal « KAFALA », lequel sera être adapté aux exigences sociétales et aux
     composantes de l'identité nationale pour les familles à l'étranger, tout en facilitant les
     procéduresy afférentes;
   - promouvoir les programmes de solidarité destinés aux jeunes par une approche basée sur
     l'écoute de leurs préoccupations, en impliquant les associations activant dans ce domaine
     (3.754 associations recensées).

2.3. Préservation et consolidation des systèmes de sécurité sociale et deretraite

La préservation et la consolidation du système de sécurité sociale et de retraite sontinscrites en
tant qu'action prioritaire du gouvernement. A ce titre, il œuvrera à l'élargissement de la base
cotisante et à l'intégration progressive des personnes actives, occupées au niveau du secteur
informel.

Des campagnes de communication sociale seront lancées pour encourager et promouvoirl'acte
d'affiliation aux caisses de sécurité sociale, en valorisantl'intérêt et l'importance de la protection
sociale pourles citoyens etleurs ayantdroits.

Dans ce cadre,il engagera le renforcement, notamment à travers l'entraide administrative, des
systèmes d'information des organismes de sécurité sociale devant permettre l'assainissement des
donnéeset un suivi rigoureux des opérations de recouvrement.

Il œuvrera également à développer des prestations de services électroniques à distance des
caisses de sécurité sociale, à adapterla fonction de recouvrementet le renforcement du contrôle
des assujettis et à développer l'évaluation médico-économique dans le domaine de l'assurance
maladie.
                                                  52
Enfin, le cadre législatif et réglementaire de la lutte contre la fraude et le contrôle médical sera
renforcé et soutenu, notammentparla mise en place dela cartographie surles pratiques d'évasion
parafiscale.

Par ailleurs, le processus de contractualisation entre les caisses de sécurité sociale et les
établissements publics de santé sera engagé, à travers la généralisation de la phase test de
facturation à blanc, avant l'entame progressive de la phase de facturation réelle.

Au tite de la retraite, le gouvernement encouragera la création de la branche retraite
complémentaire au sein de la CNR et de la CASNOSpour améliorer le pouvoir d'achat des
retraités bénéficiaires et contribuer au redressement des équilibres financiers de la branche
retraite.

2.4. Accès au logement

En la matière, le gouvernement reste déterminé à trouver les réponses idoines en garantissant
l'accès du citoyen à un logement décent, selon des formules adaptées, en ciblant en priorité les
ménagesà faibles revenus.

Dans ce cadre,il s'engage à intensifier la production de logements, à mobiliser et à orienter plus
efficacementles ressources financières nécessaires et à assurerl'équité et la justice sociale. À ce
titre, le gouvemement œuvrera à :

   - l'éradication totale des bidonvilles, à travers le recensementet la mise en œuvre de nouveaux
    programmes destinés au relogement des ménages, en mettant en place un système de veille
    contreles tentatives de prolifération et derécidive;
   - la prise en charge du vieux bâti, par la consécration de mécanismes de spécialisation des
     différents intervenants et la mobilisation des ressourcesfinancières, en impliquant les citoyens
    concernés;
   - la mise en place de moyens adéquats pour lutter contre la fraude en matière d'attribution de
     logements, parle renforcement du dispositif d'attribution et du contrôle préalable ;
   - l'accélération de la réalisation des projets de logements en cours et le respect des délais de
     réalisation, parle biais d'une responsabilisation financière des entrepreneurs.
   -le lancement d'un nouveau programme de 1.000.000 de logements, tous segments
     confondus, avec les équipements d'accompagnementetles viabilités nécessaires, durant le
     quinquennat 2020-2024 où l'accent sera mis sur les segments d'habitat rural et d'auto-
    construction dansles lotissements sociaux ;
   -la création d'un marché immobilier, notamment dans le domaine de la location, à travers
    l'adoption d'un cadre juridique approprié et desincitations financières. || œuvrera à encadrer
    et encourager le marché locatif immobilier en impliquant les différents intervenants et en
    mobilisant le potentiel foncier public comme moyen d'encouragement aux acteurs de la
    promotion immobilière.

2.5. Accès à l’eau potable et à l'énergie

En matière d’eau potable

L'engagement du président de la République « d'en finir avec les coupures d'eau et de garantir un
égal accès des citoyens aux différents services del’eau à traversl'ensemble duterritoire national »
sera unepriorité du gouvemement pourles cinq années à venir.


                                                 53
Du fait de son impact direct sur la qualité de vie des citoyens et de son rôle dans
l'accompagnement du développement socioéconomique du pays, l'accès à l'eau bénéficiera du
total soutien de l'État.

Les conséquences des changements climatiques commandent, aujourd'hui, d'optimiser
l'exploitation du potentiel hydrique, d'augmenter les capacités de mobilisation conventionnelle et
non conventionnelle et de se prémunir au mieux desrisques majeurs liés à l'eau, du gaspillage et
dela pollution de cette ressource vitale.

Le plan d'action du gouvernement englobela combinaison du dessalement pourla bandelittorale,
des interconnexions entre les barrages et les systèmes de transferts, de l'utilisation systématique
des eaux et des boues épurées dans l'industrie et l’agriculture et, enfin, de l'exploitation des
ressources souterraines dormantes du Sahara septentrional.

Lesefforts de mobilisation et l'usage optimal de la ressource seront plus soutenus avecl'objectif
de réduire la proportion des eaux impactées par l'aléa climatique en développant, là où elles
s'avèrerontréellement utiles et dans le strict respect del'équilibre des écosystèmes en place,les
ressources non conventionnelles, notamment par la réalisation de nouvelles stations de
dessalement d'eau de meret destations de déminéralisation dans les régions sud.

La capacité nationale de stockage des eaux superficielles sera portée à 10 milliards m3 par        la
réalisation et la mise en service de nouveaux barrages et l'interconnexion des grands ouvrages     et
complexes hydrauliques du pays. La cadence de réalisation de forages à destination agricole        et
domestique sera accélérée, en veillant à préserverles nappes phréatiques de la surexploitation     et
en permettant leur régénération.

La stratégie de mobilisation ne selimitera pas à ces grands captages; elle intègre aussiles niches
hydriques, tels que les petits barrages et les retenues collinaires qui constituent des ressources
non négligeables, notamment en zones éparses et en milieu rural.

Ainsi, une attention particulière sera réservée à l'amélioration du service public del'eau potable et
del'assainissement, à travers:

   - la maintenance et la réhabilitation des réseaux et des équipements pourl'amélioration des
     rendements techniques et commerciaux avec une utilisation plus étendue des énergies
     renouvelables;
   - Un programme particulier de rattrapage pourles wilayas et zones qui enregistrent des déficits
     importants et des perturbations d'alimentation ;
   -le renforcement des mesures en matière d'économie et de préservation de la ressource
     (mesures coercitives, prévues par la loi relative à l'eau et incitatives : maitrise des tarifs,
     campagnes de sensibilisation) et la lutte contre les gaspillages et les piquagesillicites de
     l'eau ;
   - l'automatisation et la numérisation des différentes tâches liées aux métiers de l'eau (SIG,
     télégestion, robotisation, compteursintelligents, etc.) et la mise à niveau du management des
     établissements publics en charge de la gestion des services del'eau ;
   - la valorisation des produits issus del'épuration (eaux et boues) avecla prise en charge de leur
     double impact environnemental et économique;
   - la densification du réseau de laboratoires d'analyse de la qualité des eaux et l'intégration des
     services et régies communalesà l'ADE et l'ONA.


                                                 54
En matière d'énergie

Les objectifs du gouvernement dans ce domaine consistent à satisfaire les besoins énergétiques
du citoyen et du marché national et à assurer la mise à niveau et le développement d'un service
public de qualité conforme aux standards internationaux.

Pour l'atteinte de ces objectifs, le plan d'action du gouvernement prévoit l'achèvement des
programmes publics en cours de réalisation, à savoir :

   - le raccordement de 398.000 foyers en gaz et 112.000 foyers en électricité ;
   - les « programmes complémentaires » qui prévoient le raccordement de 10.000 foyers en gaz
    et 7.000 foyers en électricité ;
   - le raccordement en électricité et en gaz de la nouvelle ville de Draa Errich (4.000 foyers en
     gaz et 5.000 foyers en électricité).

Le plan d'action concernantles futurs programmes publics d'électrification et distribution publique
du gaz sera mis en œuvre, selon l'approchesuivante :
   - l'alimentation en électricité des zones éparses et déshéritées dans le cadre de programmes
     spécifiques d'électrification (conventionnelle ou solaire);
   - le raccordement desvilles nouvelles, pôles urbains et zonesindustrielles.

2.6. Mobilité et transport

Les objectifs du gouvernement dans ce domaine, vecteur de développement économique et social
et d'amélioration du cadre de vie du citoyen, visent à répondre avec efficacité aux besoins de
déplacement des personnes et des transports de marchandises par l'amélioration des conditions
de mobilité en développant des modes de transport moderneet efficace, répondant aux critères de
sécurité, de confort, de gain de temps, de coût et d'écologie.

Les différents réseaux bénéficieront d'un meilleur maillage pour la desserte du territoire par des
actions de désenclavementet d'amélioration de l'accessibilité des zones de montagnes, des Hauts
Plateaux et du Sud, contribuantà la fixation des populations.

Une politique d'intermodalité des transports sera développée à travers la construction de réseaux
routiers etferroviaires, structurants et modemes, desservant notammentles ports, les aéroports et
les zoneslogistiques.

Le gouvernement veillera à la sauvegarde et à la modernisation des infrastructures de transports
ainsi que la réalisation de nouvelles infrastructures dédiées, particulièrement, au commerce, aux
services, au développementdel'activité de la pêche et au tourisme.

Il s'engage par ailleurs, à renforcerles réseaux d’infrastructures qui concourent à l'amélioration du
cadre devie du citoyen, en matière de transport, notamment:

   -la poursuite de la réalisation de projets d'augmentation de capacité du réseau par son
    dédoublement, des évitements des grandes agglomérations et d'aménagements urbains pour
    la décongestion dela circulation ;
   - l'achèvementetl'engagement des travaux des extensions deslignes du Métro d'Alger et de
     lignes de tramway existantes et de réalisation de nouvelleslignes;

                                                  55
   - l'achèvementdes travaux de réhabilitation et la remise à niveau technologique de télécabines
     et téléphériques;
   - l'achèvement des travaux de l'autoroute Est-Ouest, la mise en place des systèmes
     d'exploitation et de péage et la transformation progressive, en axe autoroutier, de la Route
    Transsaharienne;
   - l'achèvement des travaux des liaisons et pénétrantes autoroutières et la sauvegarde du
     réseau routier par des projets d'entretien et de réhabilitation ;
   - l'introduction de systèmes intelligents de gestion du trafic au niveau des grandesvilles et la
     mise en œuvre du programme de modernisation des outils de production des transports des
     voyageurs et de marchandises;
   - l'amélioration des conditions d'accueil au niveau des gares, le rehaussement du niveau de
     qualité des prestations à bord destrains et la mise en œuvre des mesures nécessaires devant
     garantir une meilleure ponctualité destrains ;
   -le lancement de programmes de réalisation d'infrastructures d'accueil et de traitement de
     voyageurs (gares routières, stations urbaines, pôles d'échanges) et l'élargissement du
     périmètre géographique de l'offre de transport parle lancement de nouvelles dessertes ;
   - l'achèvement du programmed'extension, d'électrification et de modernisation du réseau ferré
    en termes de dédoublement de voies, ainsi quela réalisation de nouvelleslignes ;
   - l'achèvement des travaux de l'aérogare et de l'aéroport international d'Oran ainsi que les
    extensions des infrastructures portuaires;
   - la réouverture à l'exploitation de l'ensemble des infrastructures aéroportuaires à la circulation
     aérienne avec un renforcementet un renouvellementde la flotte aérienne du pavillon national.

Enfin, le gouvernement œuvrera également à améliorerla qualité de service des transports publics
et l'efficacité des systèmes en place, en termes de fréquence, de durée de déplacements, de
confort et de sécurité à travers, notamment le renforcement des missions des autorités
organisatrices du transport urbain. À cetitre, il s'engage à :

   -renforcer le dispositif réglementaire régissant l'activité de transportet moderniser
     l'administration par la numérisation des services et la simplification des procédures ;
   - réaliser des études de mobilité (plans de transport et de circulation), en vue de répondre
     efficacementà la demande de transport d'une manière organiséeet ciblée et pour lutter contre
     les phénomènes dela congestion et de la pollution et s'inscrire ainsi dans le développement
     durable et l'amélioration de la qualité de vie descitoyens.

3. Pour un cadre de vie de qualité :

3.1. Aménagementduterritoire et projets intégrés

Les objectifs du gouvernementvisent « la mise en œuvre d'unevéritable politique d'aménagement
du territoire » qui assure un environnement urbain et rural préservé, valorisé et propice à
l'épanouissement du citoyen, lui assurant un cadre de vie de qualité, respectueux des exigences
de développement durable.

A cetitre, les actions du plan d'action du gouvernementportent notammentsur:
   - le renforcement du dispositif relatif à l'aménagement etl'urbanisme et à la politique dela ville,
     pour intégrer toutes les dimensions environnementales, sociologiques et paysagères,
     s'appuyant sur les fondements et principes des objectifs de développement durable et du
    nouvel agenda urbain mondial;



                                                   56
   - la réalisation des villes nouvelles, en intégrantl'ensemble des exigences de développement
     durable, notammentle principe d'efficacité énergétique et l'encouragement del'utilisation des
     énergies renouvelables et del'investissement, conformémentà leurs plans d'aménagementet
     à leurs vocations, ainsi que l'adaptation et la mise à niveau desvilles ;
   - la création, à court terme, de nouveaux lotissements sociaux satellitaires et de nouveaux
    pôles autour des grandesvilles du Nord et du Sud, pour un meilleur aménagementterritorial et
    une meilleure répartition de la population et ce, à travers:
   +   l'intensification de la création delots sociaux comme moyen de répondre aux demandes en
       logements dans les wilayas du Sud et des Hauts Plateaux ;
   e   la création de nouveaux pôles urbains dansle respect de la politique de l'aménagement du
       territoire.

Parailleurs, le plan d'action du gouvernement prévoit également :

   - l’actualisation du SNATafin de réajuster la stratégie nationale d'aménagement du territoire,
     dans le contexte socio-économique et politique actuel ainsi que l'actualisation des Schémas
     Directeurs d'Aménagement des Aires Métropolitaines (SDAAM);
   - la révision du Plan d'Aménagementdu Territoire de Wilaya (PATW) afin de définir une vision
     globale collective et partagée et proposer un cadre de référence pourles futures politiques de
     développement et d'aménagement du territoire et de promouvoir une stratégie à moyen et
     long termes pourle territoire de la wilaya ;
   - l'adaptation du Plan d'Aménagement et de Développement du Territoire de la Commune
     (PADTC) afin de renforcerl'efficacité et la cohérence de l'action publique au niveau de cet
     espace pour un aménagement de proximité ;
   -le rééquilibrage entre les intérêts des populations qui vivent dans les espaces
     sensibles (zones frontalières, espaces oasiens, montagnesetlittoral) et la protection de ces
     milieux trèsfragiles, à travers des études d'aménagement des espaces ;
   - le renforcement du Schéma Directeur d'Aménagement du Littoral (SDAL) qui constitue un outil
     technique, institutionnel et juridique, afin de maîtriser la croissance des zoneslittorales et
     d'assurer un développementplus qualitatif ;
   - la valorisation et l'aménagement des écosystèmes oasiens, à travers des projets intégrantles
     secteurs clés pour un développement local adapté aux potentialités des ces territoires
     et stabilisation des populations, parl'amélioration de leurs conditions socio-économiques;
   - le développement économique durable des massifs montagneux :
   - l'identification et le développement des zones à promouvoir et le développement des Schémas
     Directeurs des grandesinfrastructures et des servicescollectifs d'intérêt national qui déclinent
     la stratégie nationale d'aménagement du territoire aux niveaux sectoriels.

3.2. Respect des règles d'urbanisme et des normes

Dans ce cadre, les actions du gouvemement visent la mise en œuvre d'une véritable politique
d'urbanisme quitient compte des normes architecturales et préservele patrimoine.

Cesactions portent notammentsur :
   -la valorisation de la production architecturale, à travers le renforcement du contexte
    règlementaire en vue del'émergence d'une architecture et d'une ingénierie de la construction
    de qualité, l'élaboration de charte d'identité architecturale régionale et l'encouragement de
    l'émergence d'œuvresarchitecturales de qualité ;

                                                 57
   -le renforcement et l'adaptation des missions d'inspection et de contrôle en matière
    d'urbanisme à travers la promotion du rôle des agents habilités, la couverture des zones
    ruraleset le suivi de l'exécution des constats d'infractions.

3.3. Environnement et développement durable

L'action du gouvernement sera orientée vers la protection et la valorisation de la nature et de la
biodiversité, les technologies vertes et l'économie circulaire, ainsi que vers la lutte contre le
réchauffementclimatique et la pollution atmosphérique.

Ainsi, la préservation del'environnementet le recours aux énergies renouvelables sontinscrits au
cœur de la politique du gouvernement comme axes stratégiques contribuant à l'essor de la
nouvelle vision de gouvernance, basée sur un rééquilibrage territorial entre le Nord et le Sud, et qui
placel'environnement saharien parmi les priorités et ce, à travers des actions de valorisation de
toutes sesrichesses.

Devantles grands défis environnementaux et énergétiques, le gouvernement s'attèlera à mettre en
œuvre une politique inclusive et participative s'inscrivant dans la durée et ce, pour garantir à
chaque Algérien le droit de vivre dans un environnement sain.

Cette politique s'articule autour de l'amélioration du cadre et de la qualité de vie du citoyen, la
préservation des ressources naturelles, le recours aux énergies renouvelables pour réduire la
dépendance     aux   énergies fossiles,     l'utilisation   des   technologies   nouvelles et,   enfin,   le
renforcement du rôle actif et participatif du citoyen.

Pour la réalisation de ces objectifs, le gouvernement s'engage à:

   - renforcerla protection del'environnement etla lutte contre toutesles formesde pollutions et à
     promouvoir des technologies de production plus propres avec la prévention et l’atténuation
     des impacts négatifs surl'environnement ;
   - actualiser et mettre en œuvre le plan national d'action des modes de consommation et de
    production durable ;
   - élaborer un plan national et des plans locaux d'adaptation aux changements climatiques et le
    renforcementde la résilience face à leurs effets;
   - préserveret valoriser la biodiversité et les écosystèmeset créer desfilières de valorisation
     des services éco-systémiques et d'extension du réseau desaires protégés;
   - protéger l'environnement saharien, la gestion intégrée des zones désertiques et à mettre en
     œuvre Un programme national d'actions relatives à la gestion intégrée des éco-systèmes
    désertiques;
   - aménager des réserves pourla protection des espèces protégéeset en voie d'extinction ;
   - assurerl'élaboration du cadastre national desrejets liquides, des pollutions atmosphériques
     urbaineset industrielles et de tous les milieux récepteurs vulnérables (réceptacles des rejets)
     et des sourcesà l'origine de cesrejets ;
   - développer les énergies renouvelables hors réseau, afin d'atteindre une production de 1000
     MW à l'horizon 2030, dont 500 MW d'ici 2024.
   - promouvoir la maitrise del'énergie ;
   - adapter les textes juridiques et les cadres institutionnel et fiscal et élaborer des textes
     réglementaires en vuedel'optimisation du recouvrement des taxes environnementales;



                                                    58
- renforcer le cadre réglementaire en incluant la certification obligatoire des installateurs et
  l'agrément des bureaux d'études et définissant des mécanismes financiers pour le
  développement des énergies renouvelables en hors réseau ;
- fédérerles efforts avec le tissu associatif activant dans le domaine de l'environnementet des
  énergies renouvelables à travers la mise en place d'une plateforme nationale dédiée aux
  partages et aux échanges;
- renforcer la coopération internationale, notammentavecles pays dela rive méditerranéenne
  et les pays voisins (région Afrique du nord et du Sahel) ainsi que la coopération avec les
  institutions et organismes internationaux ;
- élaborer une stratégie nationale d'information, de sensibilisation,       d'éducation et de
 communication soutenue surl'environnement.




                                             59
                                     CHAPITRE QUATRIEME
              POUR UNE POLITIQUE ETRANGERE DYNAMIQUE ET PROACTIVE


Sous la conduite du président de la République, la politique extérieure de notre pays s'inscrit
naturellement dans le prolongement de la politique de redressement national qu'il s'est engagé à
mettre en œuvre au plan interne. Notre diplomatie sera évidemment et tout naturellement au
service de notre politique globale de renouveau nationalet d'édification d'une nouvelle République.
Elle consistera avanttout à assurerà l'Algérie la place etle rôle qui lui siéent dans le concert des
nations, à la hauteur de sa vocation de « puissance régionale ».

Fidèle à sa doctrine et ses principes cardinaux qui l'ont de tout temps sous-tendu, à savoir le
respect de la souveraineté, de l'indépendance et de l'intégrité des Etats, la non-ingérence dans les
affaires intérieures des Etats, le respect des règles de bon voisinage, le règlement pacifique des
différends et le soutien aux causes justes, l'action diplomatique de l'Algérie se déploiera sous le
triptyque « souveraineté, sécurité et développement » pourla défense des intérêts suprêmes dela
Nation, la préservation de notre sécurité nationale et de notre indépendance de décision et pourla
mobilisation du partenariat étranger et de la coopération au service du développement de notre
pays.

Pour ce faire, le gouvernement œuvrera à la concrétisation des grandes orientations et des
priorités de la politique extérieure en accordant un intérêt particulier à l'Afrique, en accentuant
notreinfluence dansnotre voisinage immédiat dansla région du Sahel et du Maghreb.

La politique extérieure de l'Algérie s'investira pleinement et en priorité dans la promotion des
dynamiques de coopération, d'association et d'intégration dans tous les ensembles auxquels
l'Algérie appartient que sont le Maghreb, le Sahel, l'Afrique, le monde arabe, la communauté
islamique etl'espace méditerranéen.

Dans cecadre,il sera mis en œuvre une stratégie de renforcement de la présence de notre pays
dans les structures exécutives des organisations régionales et internationales dontil est membre.
Il sera procédé égalementà l'évaluation desrelations avecles partenaires stratégiquesdel'Algérie
pourpositionnernotre pays en tant que puissance d'équilibre. Les considérations de pragmatisme
et de préservation des intérêts de notre pays doivent désormais présider aux interactions avec les
partenaires étrangers del'Algérie dans une approche gagnant-gagnant.

L'appareil diplomatique connaitra une refonte pour qu'il soit à même de s'acquitter de ses missions
classiques ainsi que de diplomatie économique dynamique au service desintérêts économiques et
de développementdel'Algérie.

Dans cecadre, des ajustements bien étudiés à l'appareil diplomatique seront opérés, y compris à
travers la révision dela carte diplomatique et des méthodes de fonctionnement, en vue de mettre à
contribution notre réseau diplomatique et consulaire pour la construction d'une économie nationale
forte etdiversifiée, génératrice de croissanceinclusive et créatrice d'emplois et de valeur ajoutée.

Cette diplomatie économique s’appuiera aussi sur une Agence de coopération et de
développement qui sera l'outil privilégié de la diplomatie économique et des actions humanitaires
de notre pays en Afrique notamment, en accompagnant et en conseillant les opérateurs
économiques algériens, publics et privés, dans la conduite de leurs investissements et échanges
commerciaux internationaux.
                                                60
Dans le même temps, la diplomatie algérienne tirera pleinement profit des espaces économiques
dans lesquels elle est partie, notamment la zone de libre-échange continentale africaine et la
grande zonedelibre-échange arabe, en accentuant son travail d'exploration et de conseil en vue
d'améliorerl'accès des opérateurs nationaux aux marchésafricains et arabes.

Notre réseau diplomatique et consulaire sera également investi d'une mission permanente
d'attraction des investissements étrangers, de promotion du marché algérien, de promotion de la
destination touristique Algérie. À cet égard, il y a lieu de releverl'importance d'une refonte des
procédures de délivrance de visas, notamment au profit des hommes d'affaires et des touristes,
l'objectif étant de parvenir à mettre en place un système de visa électronique (e-visa).

Il sera également procédé à la mise en place d'une diplomatie culturelle au service du
rayonnementet del'influence de notre paysà l'international, notamment par le développement du
réseau des centres et Instituts culturels algériens et des écoles algériennesà l'étranger.

Corrélativement, la diplomatie cultuelle sera également un levier d'action en faveur du
rehaussementde notre présence cultuelle en Afrique et dans le monde ainsi que pour contribuerà
promouvoir un Islam authentique, qui a historiquement prévalu dans notre pays, et un discours
religieux tolérant et modéré.

La communauté nationale à l'étranger sera au cœur des préoccupations de la politique extérieure.
Le gouvernement veillera à la protection des ressortissants nationaux dans le monde, à la
préservation de leurs droits et intérêts, leur sécurité et leur dignité dans les pays d'accueil. Il
œuvrera également au renforcementdela relation de nos compatriotes l'étranger avec la mère
patrie et à la prise en compte de leurs préoccupations au plan interne, notammentla circulation
despersonnesetle rapatriement des dépouilles d'algériens décédésà l'étranger.

L'action du gouvernementen direction de notre communauté l'étranger sera renforcée à travers
la modernisation de l'administration consulaire et l'introduction du numérique et de l'administration
électronique ainsi que la mise en œuvre d'une politique visant à faire émerger un mouvement
associatif représentatif et dynamique.

Plus particulièrement, le gouvernements’attellera à mobiliser et à impliquer les compétences
nationales à l'étranger dans le processus de renouveau national à travers des mécanismes
appropriés.




                                                 61
                                     CHAPITRE CINQUIEME
            RENFORCEMENT DE LA SECURITE ET DE LA DEFENSE NATIONALE



En matière de défense nationale, l'Armée Nationale Populaire agissant dans le cadre de ses
missions constitutionnelles de défense et de souveraineté nationales ainsi que de préservation de
l'intégrité du territoire national, sous la direction du président de la République, Chef suprême des
Forces Armées, Ministre de la Défense Nationale, poursuivra les efforts entrepris en matière de
modernisation et de professionnalisation de ses composantes.

Les événements qui affectent certains pays frontaliers exigent le renforcementde la sécurisation
desfrontières terrestres du pays, notammentcelles du Sud et du Sud-est et des espaces maritime
et aérien, à travers la mise en œuvre de plans d'action appropriés et une montée en puissance des
moyens humains et matériels, proportionnelle à l'ampleur de la menace, de manière à pouvoir faire
face à toute éventualité.

Le gouvernement soutiendra l'Armée Nationale Populaire, appuyée par les autres services de
sécurité, dans l'intensification, sans relâche, de la lutte contre les résidus du terrorisme jusqu'à
l'éradication définitive de ce phénomèneétranger à nos coutumes et valeurs ancestrales ainsi que
contrele crime transfrontalier,le trafic de drogue,la contrebandeet l'immigration clandestine etce,
dans le strict respect des engagements de notre pays et de son attachement à la promotion de la
paix aux niveaux régionalet international.

Dans ce cadre, la coopération avec les pays voisins en matière de lutte contre le terrorisme et la
subversion, ainsi que la criminalité transfrontalière sous toutes ses formes, aussi bien dans un
cadre bilatéral que multilatéral, sera développée et renforcée, à travers des mécanismes
opérationnels adaptés.

Le gouvernement appuiera le développement des secteurs de l'industrie et de recherche-
développement de défense, résolument orientés vers la maîtrise de la technologie et le savoir-
faire, qui connaissent une dynamique et une impulsion nouvelle en termesde partenariat diversifié,
de mobilisation de toutes les capacités nationales et une intégration harmonieuse l'outil industriel
national, de nature à contribuer au développement del'économie nationale.

L'Armée Nationale Populaire, guidée par les principes et objectifs de politique étrangère et de
coopération nationale, développera les actions de coopération internationale bilatérale et
multilatérale dans le domaine militaire, dans le but d'accroitre ses moyens en équipements et ses
capacités d'action, pour promouvoir les espaces de sécurité et de stabilité et apporter sa
contribution aux opérations de maintien de la paix, notamment en Afrique, sous l'égide des
organisations internationales.

Le gouvernement renforcera les capacités de cyber défense de l'Armée Nationale Populaire lui
permettant une mise en œuvreefficace de la stratégie nationale de la sécurité des systèmes
d'information pourla protection et la sécurisation de l'ensemble desinstitutions et organismes de
l'Etat contre toute menaceen la matière.




                                                 62
En outre,l'Armée Nationale Populaire poursuivra sa contribution au titre de la politique menée par
l'Etat dans le domaine du désenclavementet dela sécurisation des zonesfrontalières du pays.

Enfin, l'Armée Nationale Populaire, digne héritière de l'Armée de Libération Nationale, participera
activement à la prise en charge médicale et sanitaire des citoyens des zones enclavées,
notamment au niveau des régions des Hauts Plateaux et du Sud, et apporteral'aide et l'assistance
requises à la population à travers tout le territoire national, à chaque fois que la situationl'exige.




                                                 63
                         PLAN D'ACTION DU GOUVERNEMENT -— FEVRIER 2020
                                 ANNEXE:
         PRINCIPAUX AGREGATSET INDICATEURS ECONOMIQ!                     ET SOCIAUX


I-   PRINCIPAUX AGREGATS MACROECONOMIQUES:

                                                               Prévision Clôture   Prévision LF
                   AGREGATS                        Unité
                                                                           2019           2020

                                             Millions USD                34531          35232

Taux de change                                 DA/USD                       120            123

Importations de marchandises                 Millions USD                44551           38 560

Produit Intérieur Brut                         Mrds DA                   20 706          21 424

Produit Intérieur Brut Hors Hydrocarbures      Mrds DA                    16502          17 050

Croissance du PIB                                   æ                        2,1            1,8

Croissance du PIB HH                                %                        2,5            1,8

Fiscalité pétrolière budgétisée                Mrds DA                     2714           2200

Taux d’inflation                                    æ                        4,5            4,1


II- DONNEES RELATIVES AU MARCHE DE L'EMPLOI

                     INDICATEURS                             Unité                  A mai 2019

Population active                                            Millier                     12730

Population occupée                                           Milier                      11281
Population en chômage                                        Millier                      1449

Taux de chômage                                                %                           11,4

Taux de chômage chez lesjeunes (16-24 ans)                     %                           26,9

III- SITUATION FINANCES PUBLIQUES ET COMMERCE EXTERIEUR :

               FINANCE:                                      Unité                    Fin 2019

Dépenses Budgétaires                                        Mrds DA                       8200
     —   Dépenses d'équipement                              MMSDA                         ol
     — Dépensesde fonctionnement                            MRSDA                         4955
Recettes Budgétaires                                        Mrds DA                       6762

Solde budgétaire                                            Mrds DA                      -1438

Solde global du Trésor                                      Mrds Da                      -2 386




                                              64
               COMMERCE EXTERIEUR                      Unité         11 mois 2019

Importations                                        Millions USD          38372

Exportations                                        Millions USD          32618

Solde Balance Commerciale                           Millions USD           -5753

Taux de couverture                                         %                  85




               SITUATION FINANCIERE                                Décembre 2019

Liquidités bancaires                                  Mrds DA               1101
Liquidités du Trésor                                  Mrds DA               1867

Crédits à l’économie                                  Mrds DA              10 858

Comptes du Trésor (3 comptes)                         Mrds DA               1774
    = Compte FN                                       Mrds DA               1185
    _ Compte FRR                                      Mrds DA                305

    — Compte Courant du Trésor                        Mrds DA                284
Emprunt du Trésor auprès de la Banque d’Algérie
                                                      Mrds DA               6556
(Financement non conventionnel)

    =   Exercice 2017                                 MÉSDA                 2185
    -   Exercice 2018                                 MRSDA                 3371
    =   Exercice 2019                                 MRSEA                 1000
Réserves de change                                   Mrds USD               62,75


IV- INFRASTRUCTURES
               SECTEUR HABITAT                       Unité

Parc existant au 31/12/2019                          Unités            10.000.000

Taux d’Occupation par Logement (TOL)                   %                      4,5
Programmesen cours au 31/12/2019                     Unités              973871

    — Logement Public Locatif (LPL)                  Unités              204 604
    —   Logement Promotionnel Aidé et Social         Unités               190477
        Participatif (LPA/LSP)
    — Logement en Location-Vente (LLV)               Unités              25
    = Habitat Rural                                  Vies                 128
                                                     Unités               21448
    —   Logement Promotionnel Public (LPP)



                                               65
                 INFRASTRUCTURES EDUCATIVES                    Parc actuel

Education Nationale

Cycle primaire                                                      19 486

Cycle moyen                                                          5647
Cycle secondaire                                                     2501

Cantinesscolaires                                                   14 879

Enseignement Supérieur

Universités                                                            50

Centres universitaires                                                  13

Ecoles Supérieures                                                     43

Résidences universitaires                                             441

Centres de recherche                                                    19

Formation Professionnelle

Instituts nationaux spécialisés de formation professionnelle

etleurs annexes                                                       75

Instituts d’enseignement professionnel                                  17
CFPA                                                                  1090



        INFRASTRUCTURES SPORTIVES ET DE JEUNES                 Parc actuel

Complexessportifs de proximité                                        656

Maisons de jeunes                                                     936

Salles polyvalentes                                                   281

Auberges de jeunes                                                    225

Ecoles de jeunes talents                                                4

Terrains sportifs de proximité                                       3533

Bassins de natation et piscine                                        379



              INFRASTRUCTURES SANITAIRES                       Parc actuel

Etablissements Publics Hospitaliers (EPH)                             206

Etablissements Hospitaliers (EH)                                       09

Etablissements Hospitaliers Universitaires (EHU)                       01

Centres Hospitalo-Universitaires (CHU)                                  15




                                                    66
Etablissements Publics de Santé de Proximité (EPSP)       273

Etablissements Hospitaliers Spécialisés (EHS)              80

Polycliniques                                            1716

Salles de soins                                          6003

Centres d’hémodialyse                                     162

Centres Anti-Cancer (CAC)                                  li




            INFRASTRUCTURES COMMERCIALES              Nombre

Marchés de gros de légumeset de fruits                     46

Marchés de détail                                        1485

Marchés hebdomadaires                                     640

Marchés de bétail                                         265

Poissonneries et marchésde poissons congelés               47

Abattoirs                                                1427

Centres commerciaux                                       150



             INFRASTRUCTURES HYDRAULIQUES             Nombre

Barragesen exploitation                                    80

Barrages en cours de réalisation                           06



                INFRASTRUCTURES ROUTIERES                KM

Autoroutes                                               1132

Voies express                                            4568

Routes nationales                                       31816

Chemins de wilayas et communaux                         96528




             INFRASTRUCTURES FERROVIAIRES

Réseau ferroviaire                                    4200 KM

Métro                                                 18,2 KM
Tramways (06)                                         95,8 KM

Gares                                                      85



                                                 67
            INFRASTRUCTURES PORTUAIRES ET
                       AEROPORTUAIRES                         Nombre
Aéroports                                                           36

    — international                                                  12
    = National                                                       19
    — Régional                                                       13
Ports                                                               51

    —   Port mixte                                                   4

    — Port à hydrocarbures                                          02
    — Port et abris de pêche                                        37
    — Port de plaisance                                             01

            INFRASTRUCTURES CULTURELLES                       Nombre

Maisons de culture                                                  45

Bibliothèquesprincipales de lecture publique                        43

Bibliothèquesde lecture publique                                   144

Théâtres régionaux                                                   17

Salles de cinéma                                                    81

Centres culturels                                                   31



                     SOLIDARITE NATIONALE                     Nombre

Centres pour enfants handicapés mentaux                            162

Ecoles pour enfants mal entendant et non-voyants                    70

Centres pour personnes handicapés moteurs                           08

Centres spécialisés de protection                                    11

Etablissements pourenfants assistés                                 53


                     AGRICULTURE ET PECHE

Potentiel agricole                                      44 Millions HA

Superficie Agricole Utile                               8,6 Millions HA

Nombre d’exploitations agricoles                              1 260 000

Plages d’échouage                                                    6

Centres de pêche continentale                                        6

Halles à marée                                                       13



                                                   68
